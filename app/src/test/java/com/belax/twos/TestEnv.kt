package com.belax.twos

import io.reactivex.Scheduler
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by Alexander on 28.02.2018.
 */
object TestEnv {
    val rxAwaitDelay = 550L
    val rxAwaitDelayUnit = TimeUnit.MILLISECONDS
}

object Delay {
    private val step = 50L
    private val intervals = Array(10) {
        it * step
    }
    fun interval(pos: Int) = intervals[pos]
}

fun <T> TestObserver<T>.customAwait() = await(TestEnv.rxAwaitDelay, TestEnv.rxAwaitDelayUnit)

fun startDelayed(action: () -> Unit, delayMills: Long, scheduler: Scheduler = Schedulers.computation()) {
    scheduler.scheduleDirect(action, delayMills, TimeUnit.MILLISECONDS)
}