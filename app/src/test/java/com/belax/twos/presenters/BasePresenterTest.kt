package com.belax.twos.presenters

import android.os.Parcelable
import com.belax.twos.Delay
import com.belax.twos.TestEnv
import com.belax.twos.startDelayed
import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.arch.schedulers.RxSchedulersTest
import com.belax.twos.base.BaseInput
import com.belax.twos.base.BaseView
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.customAwait
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit

/**
 * Created by Alexander on 28.02.2018.
 */
class BasePresenterTest {

    interface EmptyView : BaseView { }
    abstract class TestPresenter<V : BaseView>(schedulers: RxSchedulers, navigation: Navigation) : RxMvpBasePresenter<V, Parcelable, BaseInput>(schedulers, navigation) {
        abstract fun testMethod(times: Int = 0)
    }
    lateinit var basePresenter: TestPresenter<EmptyView>
    val schedulers = RxSchedulersTest()
    @Mock
    lateinit var view: EmptyView
    @Mock
    lateinit var navigation: Navigation

    @Before
    fun beforeEachTest() {
        MockitoAnnotations.initMocks(this)
        doNothing().`when`(view).initializeView(Mockito.anyInt())
    }

    @Test
    fun callNavigationIfViewAttached() {
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                ifViewAttached { this.navigation.goBack() }
            }
        }
        basePresenter.attachView(view)
        basePresenter.testMethod()
        verify(navigation, times(1)).goBack()
        verifyNoMoreInteractions(navigation)
    }

    @Test
    fun dontCallNavigationIfViewNotAttached() {
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                ifViewAttached { this.navigation.goBack() }
            }
        }
        basePresenter.testMethod()
        verify(navigation, times(0)).goBack()
        verifyNoMoreInteractions(navigation)
    }

    @Test
    fun callSimpleSingle() {
        val testObserver = TestObserver.create<Int>()
        val firstInt = 123
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                Single.just(firstInt).subscribe(testObserver)
            }
        }
        basePresenter.testMethod()
        testObserver.customAwait()
        testObserver.assertComplete()
        testObserver.assertValue(firstInt)
    }

    @Test
    fun callStartNewSubscribeTag() {
        val testObserver = TestObserver.create<Int>()
        val inputs = listOf<Int>(123, 456)
        val tag = "tag"
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                Single.just(inputs[times]).delay(Delay.interval(4), TimeUnit.MILLISECONDS).subscribeTag(tag, TaskType.START_NEW, testObserver::onNext)
            }
        }
        basePresenter.testMethod(0)
        startDelayed({ basePresenter.testMethod(1) }, Delay.interval(2))

        testObserver.awaitCount(1)
        testObserver.assertValue(inputs[1])
        testObserver.assertValueCount(1)
    }

    @Test
    fun callIgnoreSubscribeTag() {
        val testObserver = TestObserver.create<Int>()
        val inputs = listOf(123, 456)
        val tag = "tag"
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                Single.just(inputs[times]).delay(Delay.interval(4), TimeUnit.MILLISECONDS).subscribeTag(tag, TaskType.IGNORE, testObserver::onNext)
            }
        }
        basePresenter.testMethod(0)
        startDelayed({ basePresenter.testMethod(1) }, Delay.interval(2))

        testObserver.awaitCount(1)
        testObserver.assertValue(inputs[0])
        testObserver.assertValueCount(1)
    }

    @Test
    fun callIgnoreSubscribeTagAfter() {
        val testObserver = TestObserver.create<Int>()
        val inputs = listOf(123, 456)
        val tag = "tag"
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                Single.just(inputs[times]).delay(Delay.interval(4), TimeUnit.MILLISECONDS).subscribeTag(tag, TaskType.IGNORE, testObserver::onNext)
            }
        }
        basePresenter.testMethod(0)
        startDelayed({ basePresenter.testMethod(1) }, Delay.interval(5))

        testObserver.awaitCount(2)
        testObserver.assertValues(*inputs.toTypedArray())
        testObserver.assertValueCount(2)
    }

    @Test
    fun callIgnoreSubscribeWithDifferentTags() {
        val testObserver = TestObserver.create<Int>()
        val inputs = listOf(123, 456)
        val tag = listOf("tag1", "tag2")
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                Single.just(inputs[times]).delay(Delay.interval(4), TimeUnit.MILLISECONDS).subscribeTag(tag[times], TaskType.IGNORE, testObserver::onNext)
            }
        }
        basePresenter.testMethod(0)
        startDelayed({ basePresenter.testMethod(1) }, Delay.interval(2))

        testObserver.awaitCount(2)
        testObserver.assertValues(*inputs.toTypedArray())
        testObserver.assertValueCount(2)
    }

    @Test
    fun callIgnoreSubscribeWithTwoLastEqualTags() {
        val testObserver = TestObserver.create<Int>()
        val inputs = listOf(123, 456, 789)
        val tag = listOf("tag1", "tag2")
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                Single.just(inputs[times]).delay(Delay.interval(4), TimeUnit.MILLISECONDS).subscribeTag(tag[times.coerceAtMost(1)], TaskType.IGNORE, testObserver::onNext)
            }
        }

        basePresenter.testMethod(0)
        startDelayed({ basePresenter.testMethod(1) }, Delay.interval(2))
        startDelayed({ basePresenter.testMethod(2) }, Delay.interval(4))

        testObserver.awaitCount(2)
        testObserver.assertValues(inputs[0], inputs[1])
        testObserver.assertValueCount(2)
    }

    @Test
    fun callStartNewSubscribeWithTwoLastEqualTags() {
        val testObserver = TestObserver.create<Int>()
        val inputs = listOf(123, 456, 789)
        val tag = listOf("tag1", "tag2")
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                Single.just(inputs[times]).delay(Delay.interval(4), TimeUnit.MILLISECONDS).subscribeTag(tag[times.coerceAtMost(1)], TaskType.START_NEW, testObserver::onNext)
            }
        }

        basePresenter.testMethod(0)
        startDelayed({ basePresenter.testMethod(1) }, Delay.interval(2))
        startDelayed({ basePresenter.testMethod(2) }, Delay.interval(4))

        testObserver.awaitCount(2)
        testObserver.assertValues(inputs[0], inputs[2])
        testObserver.assertValueCount(2)
    }

    @Test
    fun onDestroyStopsAllTask() {
        val testObserver = TestObserver.create<Int>()
        val inputs = listOf(123, 456, 789)
        val tag = listOf("tag1", "tag2", "tag3")
        basePresenter = object : TestPresenter<EmptyView>(schedulers, navigation) {
            override fun testMethod(times: Int) {
                Single.just(inputs[times]).delay(Delay.interval(4), TimeUnit.MILLISECONDS).subscribeTag(tag[times], TaskType.START_NEW, testObserver::onNext)
            }
        }

        basePresenter.testMethod(0)
        startDelayed({ basePresenter.testMethod(1) }, Delay.interval(1))
        startDelayed({ basePresenter.testMethod(2) }, Delay.interval(2))
        startDelayed({ basePresenter.destroy() }, Delay.interval(3))

        testObserver.await(TestEnv.rxAwaitDelay, TestEnv.rxAwaitDelayUnit)
        testObserver.assertNoValues()
    }

}