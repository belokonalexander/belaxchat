package com.belax.twos.business.entities

import android.annotation.SuppressLint
import android.os.Parcelable
import com.belax.twos.data.database.Scrap
import kotlinx.android.parcel.Parcelize

const val NO_SCRAP_SELECTED = -1

@SuppressLint("ParcelCreator")
@Parcelize
data class CameraSettings(
        val scrapbookId: Int? = null,
        val cameraType: CameraType = CameraType.MAIN,
        val resolutionFactor: Float = 1f,
        val selectedScrapId: Int = NO_SCRAP_SELECTED
) : Parcelable