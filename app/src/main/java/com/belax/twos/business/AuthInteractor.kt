package com.belax.twos.business

import com.belax.twos.data.AuthRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import java.util.concurrent.TimeUnit


interface AuthInteractor {
    fun signIn(login: String, password: String): Completable
    fun requestPhoneCode(phoneNumber: String): Completable
    fun verifyCodeAndSignIn(phoneCode: String): Completable
}

class AuthInteractorImpl(private val authRepository: AuthRepository) : AuthInteractor {

    var lastVerificationId: String = ""

    override fun verifyCodeAndSignIn(phoneCode: String): Completable {
        return authRepository.signUpWithCode(lastVerificationId, phoneCode).toCompletable()
    }

    override fun requestPhoneCode(phoneNumber: String): Completable {
        return authRepository.requestPhoneCode(phoneNumber).map { lastVerificationId = it }.toCompletable()
    }

    override fun signIn(login: String, password: String): Completable {
        return authRepository.signIn(login, password).toCompletable()
    }


}