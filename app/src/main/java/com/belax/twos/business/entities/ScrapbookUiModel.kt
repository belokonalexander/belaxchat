package com.belax.twos.business.entities

import android.annotation.SuppressLint
import android.os.Parcelable
import com.belax.twos.presentation.adapters.base.UiItem
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * Created by Alexander on 19.03.2018.
 */
@Parcelize
data class ScrapbookUiModel(
        override val id: Int = EMPTY_VAL,
        val title: String = "",
        val description: String = "",
        var scraps: Set<ScrapUiModel> = emptySet(),
        val created: Date = Date()
) : UiItem, Parcelable {
    companion object {
        const val EMPTY_VAL = 0
    }
}

@Parcelize
data class ScrapUiModel(
        override val id: Int = EMPTY_VAL,
        val title: String = "",
        val picture: String = "",
        val scrapbookId: Int = 0,
        val created: Date = Date()
) : UiItem, Parcelable {
    companion object {
        const val EMPTY_VAL = 0
    }
}