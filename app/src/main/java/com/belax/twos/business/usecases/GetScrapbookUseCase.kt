package com.belax.twos.business.usecases

import com.belax.twos.data.GalleryRepository
import com.belax.twos.data.database.ScrapbookAndScraps
import io.reactivex.Single

interface GetScrapbookUseCase {
    fun getScrapbook(scrapbookId: Int): Single<ScrapbookAndScraps>
}

class GetScrapbookUseCaseImpl(private val galleryRepository: GalleryRepository) : GetScrapbookUseCase {
    override fun getScrapbook(scrapbookId: Int): Single<ScrapbookAndScraps> {
        return galleryRepository.getScrapbook(scrapbookId)
                .map { it.apply { scraps = scraps.sortedByDescending { it.created } } }
    }
}