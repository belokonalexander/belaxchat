package com.belax.twos.business

import android.util.Base64
import com.belax.twos.base.PermissionInteractor
import com.belax.twos.base.PermissionInteractorImpl
import com.belax.twos.data.CacheRepository
import com.belax.twos.data.GalleryRepository
import com.belax.twos.data.PermissionsRepository
import com.belax.twos.data.TimeRepository
import com.belax.twos.data.database.Scrap
import com.belax.twos.data.database.Scrapbook
import io.reactivex.Completable
import io.reactivex.Single

interface PreviewInteractor : PermissionInteractor {
    fun addItemToProject(scrapbookId: Int?, key: String): Single<Scrap>
    fun getPreviewImage(key: String): Single<ByteArray>
}

class PreviewInteractorImpl(
        private val galleryRepository: GalleryRepository,
        private val timeRepository: TimeRepository,
        private val cacheRepository: CacheRepository,
        permissionsRepository: PermissionsRepository
) : PermissionInteractorImpl(permissionsRepository), PreviewInteractor {

    override fun getPreviewImage(key: String): Single<ByteArray> {
        return cacheRepository.get(key)
    }

    override fun addItemToProject(scrapbookId: Int?, key: String): Single<Scrap> =
            cacheRepository.get(key)
                    .flatMap { galleryRepository.savePicture(timeRepository.currentTime().toString(), it) }
                    .flatMap { galleryRepository.createScrap(scrapbookId ?: Scrapbook.EMPTY_VAL, it) }
}