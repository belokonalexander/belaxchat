package com.belax.twos.business

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import com.belax.twos.base.PermissionInteractor
import com.belax.twos.base.PermissionInteractorImpl
import com.belax.twos.business.entities.CameraSettings
import com.belax.twos.business.entities.CameraType
import com.belax.twos.business.usecases.GetScrapbookUseCase
import com.belax.twos.data.CacheRepository
import com.belax.twos.data.GalleryRepository
import com.belax.twos.data.PermissionsRepository
import com.belax.twos.data.database.ScrapbookAndScraps
import io.reactivex.Single
import io.reactivex.rxkotlin.toSingle
import java.io.ByteArrayOutputStream


/**
 * Created by Alexander on 09.02.2018.
 */
interface CameraInteractor : PermissionInteractor {
    fun getCameraSettings(scrapbookId: Int?): Single<CameraSettings>
    fun updateCameraSettings()
    fun preparePicture(byteArray: ByteArray, orientation: Int, cameraSettings: CameraSettings): Single<String>
    fun getScrapbook(scrapbookId: Int): Single<ScrapbookAndScraps>
}

class CameraInteractorImpl(
        permissionsRepository: PermissionsRepository,
        private val getScrapbookUseCase: GetScrapbookUseCase,
        private val cacheRepository: CacheRepository
) : PermissionInteractorImpl(permissionsRepository), CameraInteractor {

    override fun getScrapbook(scrapbookId: Int) = getScrapbookUseCase.getScrapbook(scrapbookId)

    override fun preparePicture(
            byteArray: ByteArray,
            orientation: Int,
            cameraSettings: CameraSettings
    ): Single<String> = Single.fromCallable {
        prepare(byteArray, orientation, cameraSettings.cameraType == CameraType.ALTERNATIVE)
    }.flatMap { resultData ->
        generateName().let { cacheName ->
            cacheRepository.add(cacheName, resultData).toSingle { cacheName }
        }
    }

    private fun generateName(): String = "_cached_preview"

    private fun prepare(byteArray: ByteArray, orientation: Int, isFrontal: Boolean): ByteArray {
        val storedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size, null)
        val mat = Matrix()
        mat.setRotate(orientation.toFloat() + if (isFrontal && orientation in arrayOf(90, 270)) 180 else 0)
        if (isFrontal) {
            mat.postScale(-1f, 1f)
        }
        val scaled = Bitmap.createBitmap(storedBitmap, 0, 0, storedBitmap.width, storedBitmap.height, mat, false)
        if (scaled != storedBitmap) storedBitmap.recycle()
        val stream = ByteArrayOutputStream()
        scaled.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val rotated = stream.toByteArray()
        stream.flush()
        stream.close()
        storedBitmap.recycle()
        scaled.recycle()
        return rotated
    }

    override fun updateCameraSettings() {}

    override fun getCameraSettings(scrapbookId: Int?): Single<CameraSettings> =
            CameraSettings(scrapbookId = scrapbookId).toSingle()

}