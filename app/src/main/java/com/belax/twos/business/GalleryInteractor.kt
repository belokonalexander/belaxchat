package com.belax.twos.business

import com.belax.twos.data.GalleryRepository
import com.belax.twos.data.database.Scrapbook
import com.belax.twos.data.database.ScrapbookAndScraps
import io.reactivex.Single

/**
 * Created by Alexander on 18.03.2018.
 */
interface GalleryInteractor {
    fun getScrapbooks(): Single<List<ScrapbookAndScraps>>
}

class GalleryInteractorImpl(
    private val galleryRepository: GalleryRepository
) : GalleryInteractor {

    override fun getScrapbooks(): Single<List<ScrapbookAndScraps>> {
        return galleryRepository.getScrapbooks().map {
            it.forEach {
                it.scraps = it.scraps.sortedByDescending { it.created }
            }
            return@map it
        }
    }

}