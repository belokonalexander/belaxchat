package com.belax.twos.business

import com.belax.twos.data.HardwareRepository
import com.belax.twos.data.PermissionStatus
import com.belax.twos.data.PermissionsRepository
import io.reactivex.Single

/**
 * Created by Alexander on 09.02.2018.
 */
interface HomeInteractor {
    fun checkCamerahardware(): Single<Boolean>
}

class HomeInteractorImpl(private val hardwareRepository: HardwareRepository) : HomeInteractor {
    override fun checkCamerahardware(): Single<Boolean> = Single.fromCallable { hardwareRepository.checkCameraHardware() }
}