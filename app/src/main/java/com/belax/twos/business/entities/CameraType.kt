package com.belax.twos.business.entities

enum class CameraType {
    MAIN {
        override fun alternative(): CameraType = ALTERNATIVE
    },
    ALTERNATIVE {
        override fun alternative(): CameraType = MAIN
    },
    UNKNOWN {
        override fun alternative(): CameraType = MAIN
    };

    abstract fun alternative(): CameraType
}