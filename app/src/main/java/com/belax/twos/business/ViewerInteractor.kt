package com.belax.twos.business

import com.belax.twos.business.usecases.GetScrapbookUseCase
import com.belax.twos.data.database.ScrapbookAndScraps
import io.reactivex.Single

interface ViewerInteractor {
    fun getScrapbook(id: Int): Single<ScrapbookAndScraps>
}

class ViewerInteractorImpl(private val getScrapbookUseCase: GetScrapbookUseCase) : ViewerInteractor {
    override fun getScrapbook(id: Int): Single<ScrapbookAndScraps> {
        return getScrapbookUseCase.getScrapbook(id)
    }
}