package com.belax.twos.presentation.adapters.base

import android.content.Context
import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.extensions.LayoutContainer

/**
 * Created by Alexander on 19.03.2018.
 */
abstract class BaseHolder<T>(view: View) : RecyclerView.ViewHolder(view), LayoutContainer {
    var item: T? = null
        private set

    protected val context: Context = view.context
    protected val res: Resources = context.resources
    open fun bindTo(item: T) {
        this.item = item
    }

    override val containerView: View = itemView
}