package com.belax.twos.presentation.signup

import android.annotation.SuppressLint
import android.os.Parcelable
import android.view.View
import com.belax.twos.R
import com.belax.twos.arch.BaseMvpViewStateController
import com.belax.twos.arch.MvpLoadView
import com.belax.twos.base.BaseInput
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.layout_sign_up.view.*

class SignUpController : BaseMvpViewStateController<SignUpView, SignUpPresenter, SignUpState, BaseInput>(), SignUpView {
    override fun getTitle() = "Sign Up"
    override fun getLayoutRes(): Int = R.layout.layout_sign_up
    override fun createPresenter(): SignUpPresenter =  component().getHomeComponent(getMainRouter(), getInnerRouter()).getSignUpPresenter()
    override fun createState(): SignUpState = SignUpState()

    override fun onAttach(view: View) {
        super.onAttach(view)
        view.buttonSignUp.setOnClickListener {
            presenter.test()
        }
    }

    override fun updateProgress(show: Boolean) {
        viewState.state.showProgress = show
    }

}

@SuppressLint("ParcelCreator")
@Parcelize
class SignUpState(var showProgress: Boolean = false) : Parcelable {

}

interface SignUpView : MvpLoadView {

}