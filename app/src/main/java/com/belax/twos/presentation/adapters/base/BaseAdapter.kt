package com.belax.twos.presentation.adapters.base

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter

/**
 * Created by Alexander on 31.01.2018.
 */
typealias AdapterDataType = List<UiItem>
class BaseAdapter(private val delegates: List<AdapterDelegate<AdapterDataType>>) : ListDelegationAdapter<AdapterDataType>() {

    init {
        delegates.forEach {
            delegatesManager.addDelegate(it)
        }
    }
}