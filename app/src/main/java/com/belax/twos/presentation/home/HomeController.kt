package com.belax.twos.presentation.home

import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.belax.twos.R
import com.belax.twos.arch.BaseMvpViewStateController
import com.belax.twos.base.BaseInput
import com.belax.twos.base.BaseView
import com.belax.twos.base.navigation.TabbedNavigationImpl
import com.belax.twos.views.BottomNavigationView
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.layout_home.view.*
import javax.inject.Inject

class HomeController : BaseMvpViewStateController<HomeView, HomePresenter, HomeState, BaseInput>(), HomeView {
    private lateinit var childRouter: Router
    @Inject lateinit var bottomNavSettings: BottomNavigationView.BottomNavSettings
    @Inject lateinit var defaultTabController: Controller
    override fun getTitle(): String = "Home"
    override fun getLayoutRes(): Int = R.layout.layout_home
    override fun createPresenter(): HomePresenter =  component().getHomeComponent(getMainRouter(), getInnerRouter()).getHomePresenter()
    override fun createState(): HomeState = HomeState()

    private fun onSelectTab(itemMeta: BottomNavigationView.ItemMeta) {
        presenter.onSelectTab(itemMeta)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return super.onCreateView(inflater, container).apply {
            childRouter = getChildRouter(homeContainer)
            component().getHomeComponent(getMainRouter(), getInnerRouter()).inject(this@HomeController)
            if (!childRouter.hasRootController()) {
                childRouter.setRoot(RouterTransaction.with(defaultTabController)
                        .tag(TabbedNavigationImpl.getTagValue(defaultTabController)))
            }
            bottomNav.setItems(bottomNavSettings)
            bottomNav.selectListener = ::onSelectTab
            bottomNav.reselectListener = { presenter.clearCurrentTabStack() }
        }
    }

    override fun selectTab(tabId: Int?) {
        view?.bottomNav?.silenceSelect(tabId)
    }

    override fun handleBack(): Boolean {
        return presenter.handleBack()
    }

    override fun onDestroy() {
        super.onDestroy()
        component().destroyHomeComponent()
    }
}

@Parcelize
class HomeState(var selectedTabId: Int? = null) : Parcelable

interface HomeView : BaseView {
    fun selectTab(tabId: Int?)
}