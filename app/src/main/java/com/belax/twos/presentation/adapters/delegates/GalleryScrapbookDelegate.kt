package com.belax.twos.presentation.adapters.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.belax.twos.R
import com.belax.twos.base.glide.GlideApp
import com.belax.twos.business.entities.ScrapbookUiModel
import com.belax.twos.dimensPx
import com.belax.twos.dpToPx
import com.belax.twos.presentation.adapters.base.BaseDelegate
import com.belax.twos.presentation.adapters.base.BaseHolder
import com.google.android.flexbox.FlexboxLayout
import kotlinx.android.synthetic.main.item_scrapbook_preview.*

/**
 * Created by Alexander on 19.03.2018.
 */
class GalleryScrapbookDelegate(private val updateCallback: (ScrapbookUiModel) -> Unit) : BaseDelegate<ScrapbookUiModel, GalleryScrapbookDelegate.GalleryScrapbookHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): GalleryScrapbookHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_scrapbook_preview, parent, false)
        return GalleryScrapbookHolder(view, parent.width).apply {
            imageLastScrap.setOnClickListener {
                item?.let {
                    updateCallback(it)
                }
            }
        }
    }

    class GalleryScrapbookHolder(
            view: View,
            private val recyclerItemWidth: Int = 0
    ) : BaseHolder<ScrapbookUiModel>(view) {

        override fun bindTo(item: ScrapbookUiModel) {
            super.bindTo(item)
            textScrapbookTitle.text = item.id.toString()
            textScrapCount.text = item.scraps.size.toString()
            val mainImageSize = res.dimensPx(R.dimen.scrapbookImageSize)
            val allowedSpace = recyclerItemWidth - 2 * res.dimensPx(R.dimen.scrapbookSizeMargin) - mainImageSize
            val rowsCount = 3
            val startScrapSize = mainImageSize / rowsCount

            val childInLine = getChildInLine(allowedSpace, startScrapSize)
            val imageDelta = getImageDelta(allowedSpace, startScrapSize)
            val size = startScrapSize + imageDelta

            flexboxLayout.removeAllViews()
            item.scraps.drop(1).take(childInLine * rowsCount).forEach {
                val imageView = ImageView(context).apply {
                    scaleType = ImageView.ScaleType.CENTER_CROP
                    layoutParams = FlexboxLayout.LayoutParams(size, size)
                    GlideApp.with(context).load(it.picture).into(this)
                    dpToPx(4).let { setPadding(it, it, it, it) }
                }
                flexboxLayout.addView(imageView)
            }

            imageLastScrap.apply {
                layoutParams = layoutParams.apply {
                    width = mainImageSize + imageDelta
                    height = mainImageSize + rowsCount * imageDelta
                }
                dpToPx(4).let { setPadding(it, it, it, it) }
            }

            item.scraps.firstOrNull()?.let {
                GlideApp.with(itemView)
                        .load(it.picture)
                        .into(imageLastScrap)
            }
        }

        private fun getChildInLine(allowedSpace: Int, startSize: Int) = allowedSpace / startSize

        private fun getImageDelta(allowedSpace: Int, startSize: Int): Int {
            val childInLine = getChildInLine(allowedSpace, startSize)
            val freeLineSpace = allowedSpace - (startSize * childInLine)
            val pivotDelta = freeLineSpace / (childInLine + 1)
            return pivotDelta
        }
    }
}