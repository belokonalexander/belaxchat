package com.belax.twos.presentation.home

import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.BaseInput
import com.belax.twos.base.navigation.TabbedNavigation
import com.belax.twos.business.HomeInteractor
import com.belax.twos.views.BottomNavigationView

class HomePresenter(
        private val interactor: HomeInteractor,
        rxSchedulers: RxSchedulers,
        navigation: TabbedNavigation
) : RxMvpBasePresenter<HomeView, HomeState, BaseInput>(rxSchedulers, navigation) {

    override fun onAttachAction() {
        if (navigation is TabbedNavigation) {
            navigation.getTabViewChanges().subscribe {
                ifViewAttached {
                    vs.selectedTabId = null
                    it.selectTab(null)
                }
            }.watchPresenter()
        }
    }

    override fun onRestoreView(isRetained: Boolean, input: BaseInput?) {
        super.onRestoreView(isRetained, input)
        ifViewAttached { it.selectTab(vs.selectedTabId) }
    }

    fun onSelectTab(tab: BottomNavigationView.ItemMeta) {
        vs.selectedTabId = tab.id
        ifViewAttached { it.selectTab(vs.selectedTabId) }
        (navigation as TabbedNavigation).switchTab(tab)
    }

    override fun handleBack(): Boolean {
        return navigation.goBack()
    }

    fun clearCurrentTabStack() {
        (navigation as TabbedNavigation).clearCurrentTab()
    }
}