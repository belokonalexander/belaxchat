package com.belax.twos.presentation.camera

import android.os.Parcelable
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.belax.twos.*
import com.belax.twos.arch.BaseMvpViewStateController
import com.belax.twos.base.BaseInput
import com.belax.twos.base.BaseView
import com.belax.twos.base.glide.GlideApp
import com.belax.twos.business.entities.CameraSettings
import com.belax.twos.business.entities.ScrapUiModel
import com.belax.twos.business.entities.ScrapbookUiModel
import com.belax.twos.presentation.adapters.base.BaseAdapter
import com.belax.twos.presentation.adapters.delegates.GalleryScrapDelegate
import com.belax.twos.presentation.camera.CameraPresenter.Companion.SCRAPBOOK_ID
import com.belax.twos.presentation.camera.CameraPresenter.Companion.SCRAP_RESULT
import com.belax.twos.views.ui.GridLMDecoration
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.layout_bottom_gallery.view.*
import kotlinx.android.synthetic.main.layout_camera.view.*

class CameraController : BaseMvpViewStateController<CameraView, CameraPresenter, CameraState, CameraViewInput>(), CameraView {

    private var bottomSheetBehaviour: BottomSheetBehavior<LinearLayout>? = null
    private var adapter: BaseAdapter? = null

    override fun getTitle(): String = "Camera"
    override fun getLayoutRes(): Int = R.layout.layout_camera
    override fun createPresenter(): CameraPresenter =
            component().getRetainComponent(getMainRouter()).getCameraPresenter()
    override fun createState(): CameraState = CameraState()
    override fun getInput(): CameraViewInput {
        val result = pollBundleResult()
        return CameraViewInput(pollBundleId() ?: result?.getInt(SCRAPBOOK_ID), result?.getParcelable(SCRAP_RESULT))
    }

    override fun onDetach(view: View) {
        view.cameraView.stop()
        super.onDetach(view)
    }

    override fun onDestroyView(view: View) {
        super.onDestroyView(view)
        bottomSheetBehaviour = null
        adapter = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return super.onCreateView(inflater, container).apply {
            btnCameraFire.setOnClickListener { presenter.takePicture() }
            bottomSheetBehaviour = BottomSheetBehavior.from(bottomSheet).apply {
                setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                    override fun onSlide(bottomSheet: View, slideOffset: Float) {}
                    override fun onStateChanged(bottomSheet: View, newState: Int) {
                        if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                            presenter.closeBottomSheet()
                        }
                        if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                            presenter.openBottomSheet()
                        }
                    }
                })
            }
            imageGalleryPreview.setOnClickListener {
                presenter.openBottomSheet()
            }

            adapter = BaseAdapter(listOf(GalleryScrapDelegate({ presenter.scrapGallerySelected(it) })))
            recyclerCameraGallery.apply {
                adapter = this@CameraController.adapter
                layoutManager = GridLayoutManager(context, resources.width() / dimensPx(R.dimen.scrapbookImageSize))
                addItemDecoration(GridLMDecoration(resources.dpToPx(8), this))
            }
        }
    }


    override fun openBottomSheet() {
        view?.let {
            bottomSheetBehaviour?.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    override fun closeBottomSheet() {
        view?.let {
            bottomSheetBehaviour?.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }


    override fun showCameraContent(cameraSettings: CameraSettings) {
        view?.apply {
            btnCameraFire.setVisible(true)
            view?.imageGalleryPreview?.setVisible(true)
            cameraView.start(cameraSettings, { presenter.changeCamera(it) })
        }
    }

    override fun takePicture(callback: (ByteArray, Int, CameraSettings) -> Unit) {
        view?.cameraView?.takePicture { b, i, s -> callback(b, i, s) }
        view?.btnCameraFire?.setVisible(false)
        view?.imageGalleryPreview?.setVisible(false)
    }

    override fun inProgress() {
        super.inProgress()
        view?.progress?.setVisible(true)
    }

    override fun outProgress() {
        super.outProgress()
        view?.progress?.setVisible(false)
    }

    override fun showGalleryPreview(scrapUiModel: ScrapUiModel?) {
        view?.imageGalleryPreview?.apply {
            if (scrapUiModel != null) {
                setVisible(true)
                GlideApp.with(this).load(scrapUiModel.picture).into(this)
            } else {
                setVisible(false)
            }
        }
    }

    override fun setGalleryItems(scraps: List<ScrapUiModel>) {
        adapter?.apply {
            items = scraps
            notifyDataSetChanged()
        }
    }
}

@Parcelize
class CameraState(
        var scrapbookId: Int? = null,
        var cameraSettings: CameraSettings? = null,
        var scrapbookUiModel: ScrapbookUiModel? = null,
        var bottomSheetOpened: Boolean = false
) : Parcelable

data class CameraViewInput(var scrapbookId: Int?, var scrapResult: ScrapUiModel?) : BaseInput()

interface CameraView : BaseView {
    fun showCameraContent(cameraSettings: CameraSettings)
    fun showGalleryPreview(scrapUiModel: ScrapUiModel?)
    fun setGalleryItems(scraps: List<ScrapUiModel>)
    fun takePicture(callback: (ByteArray, Int, CameraSettings) -> Unit)
    fun openBottomSheet()
    fun closeBottomSheet()
}