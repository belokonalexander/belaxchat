package com.belax.twos.presentation.signin

import android.annotation.SuppressLint
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.belax.twos.R
import com.belax.twos.arch.BaseMvpViewStateController
import com.belax.twos.base.BaseInput
import com.belax.twos.base.BaseView
import com.belax.twos.base.ProgressViewState
import com.belax.twos.setVisible
import com.belax.twos.string
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.layout_sign_in.view.*

class SignInController : BaseMvpViewStateController<SignInView, SignInPresenter, SignInState, BaseInput>(), SignInView {

    override fun getTitle(): String = "Sign in"
    override fun getLayoutRes(): Int = R.layout.layout_sign_in
    override fun createPresenter(): SignInPresenter =  component().getHomeComponent(getMainRouter(), getInnerRouter()).getSignInPresenter()
    override fun createState(): SignInState = SignInState()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return super.onCreateView(inflater, container).apply {
            buttonSignIn.setOnClickListener {
                presenter.signIn("", "")
            }
        }
    }

    override fun inProgress() {
        super.inProgress()
        view?.progress?.setVisible(true)
    }

    override fun outProgress() {
        super.outProgress()
        view?.progress?.setVisible(false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        with(view) {
            buttonSignIn.setOnClickListener { presenter.signIn(editLogin.string(), editPassword.string()) }
            buttonGoSignUp.setOnClickListener { presenter.goSignOut() }
        }
    }

    override fun startTask(flag: Boolean) {

    }
}

@SuppressLint("ParcelCreator")
@Parcelize
data class SignInState(
        override var progress: Boolean = false
) : Parcelable, ProgressViewState


interface SignInView : BaseView {
    fun startTask(flag: Boolean)
}


