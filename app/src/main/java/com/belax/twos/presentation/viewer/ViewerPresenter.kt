package com.belax.twos.presentation.viewer

import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.business.ViewerInteractor

class ViewerPresenter(private val interactor: ViewerInteractor, navigation: Navigation) : RxMvpBasePresenter<ViewerView, ViewerState, ViewerInput>(navigation = navigation) {

    override fun onAttachAction() {
        super.onAttachAction()
        checkStoredValue(vs.scrapbookUiModel.id)
    }

    override fun onRestoreView(isRetained: Boolean, input: ViewerInput?) {
        super.onRestoreView(isRetained, input)
        showCurrentScrap()
    }

    override fun mergeStateWithArgs(input: ViewerInput) {
        super.mergeStateWithArgs(input)
        input.scrapbookUiModel?.let { vs.scrapbookUiModel = it }
        input.selectedScrapId?.let { vs.selectedScrapId = it }
    }

    private fun showCurrentScrap() {
        ifViewAttached {
            it.showPictures(vs.scrapbookUiModel.scraps.toList(),
                    vs.scrapbookUiModel.scraps.indexOfFirst { it.id == vs.selectedScrapId })
        }
    }

    private fun checkStoredValue(scrapbookId: Int) {
        interactor.getScrapbook(scrapbookId).compose(schedulers.getIOToMainTransformerSingle())
                .map(mapper.toScrapbookUi())
                .subscribe({
                    if (vs.scrapbookUiModel != it) {
                        vs.scrapbookUiModel = it
                        showCurrentScrap()
                    }
                }, ::onError).watchPresenter()
    }

    fun pageSelected(position: Int) {
        vs.selectedScrapId = vs.scrapbookUiModel.scraps.toList()[position].id
    }

}
