package com.belax.twos.presentation.viewer

import android.graphics.Color
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.belax.twos.R
import com.belax.twos.arch.BaseMvpViewStateController
import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.base.BaseInput
import com.belax.twos.base.BaseView
import com.belax.twos.base.glide.GlideApp
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.base.navigation.TabbedNavigation
import com.belax.twos.business.entities.ScrapUiModel
import com.belax.twos.business.entities.ScrapbookUiModel
import com.belax.twos.business.usecases.GetScrapbookUseCase
import com.belax.twos.data.GalleryRepository
import com.belax.twos.data.database.ScrapbookAndScraps
import com.belax.twos.logExeption
import io.reactivex.Single
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.layout_viewer.view.*

class ViewerController : BaseMvpViewStateController<ViewerView, ViewerPresenter, ViewerState, ViewerInput>(), ViewerView {
    override fun createState() = ViewerState(ScrapbookUiModel(), 0)
    override fun getInput() = ViewerInput(pollBundleParcelable(), pollBundleId())
    override fun getTitle(): String = "Viewer"
    override fun getLayoutRes(): Int = R.layout.layout_viewer
    override fun createPresenter(): ViewerPresenter = component().getRetainComponent(getMainRouter()).getViewerPresenter()

    private var adapter: BasePagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return super.onCreateView(inflater, container).apply {
            this@ViewerController.adapter = BasePagerAdapter()
            pager.adapter = this@ViewerController.adapter
            pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {

                }
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }
                override fun onPageSelected(position: Int) {
                    presenter.pageSelected(position)
                }
            })
        }
    }

    override fun initializeView(stackSize: Int) {
        super.initializeView(stackSize)
        toolbar?.setBackgroundColor(Color.TRANSPARENT)
    }

    override fun showPictures(scraps: List<ScrapUiModel>, selectedPosition: Int) {
        adapter?.items = scraps
        view?.pager?.setCurrentItem(selectedPosition, false)
        adapter?.notifyDataSetChanged()
    }

    override fun onDestroyView(view: View) {
        super.onDestroyView(view)
        adapter = null
    }
}

class BasePagerAdapter : PagerAdapter() {
    var items = listOf<ScrapUiModel>()

    override fun isViewFromObject(view: View, item: Any): Boolean {
        return (item as ScrapUiModel).id == view.id
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return items[position].title
    }
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        container.addView(ImageView(container.context).apply {
            id = items[position].id
            scaleType = ImageView.ScaleType.CENTER_INSIDE
            GlideApp.with(this).load(items[position].picture).into(this)
        })
        logExeption("instantiateItem: $position, container: ${container}")
        return items[position]
    }

    override fun startUpdate(container: ViewGroup) {
        super.startUpdate(container)
        logExeption("start update")
    }

    override fun finishUpdate(container: ViewGroup) {
        super.finishUpdate(container)
        logExeption("finish update")
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.findViewById<View>(items[position].id).let { container.removeView(it) }
        logExeption("destroyItem: $position")
    }

    override fun getCount(): Int {
        return items.count()
    }
}

interface ViewerView: BaseView {
    fun showPictures(scraps: List<ScrapUiModel>, selectedPosition: Int)
}

data class ViewerInput(var scrapbookUiModel: ScrapbookUiModel?, var selectedScrapId: Int?) : BaseInput()

@Parcelize
data class ViewerState(var scrapbookUiModel: ScrapbookUiModel, var selectedScrapId: Int): Parcelable