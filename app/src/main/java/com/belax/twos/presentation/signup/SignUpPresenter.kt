package com.belax.twos.presentation.signup

import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.arch.schedulers.RxSchedulersImpl
import com.belax.twos.base.BaseInput
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.business.AuthInteractor
import timber.log.Timber

class SignUpPresenter(
        private val interactor: AuthInteractor,
        navigation: Navigation,
        schedulers: RxSchedulers = RxSchedulersImpl()
) : RxMvpBasePresenter<SignUpView, SignUpState, BaseInput>(schedulers, navigation) {

    companion object {
        val SIGN_IN_TAG = "SIGN_IN"
    }

    fun phoneSignUp(phone: String) {
        interactor.requestPhoneCode(phone)
                .compose(schedulers.getIOToMainTransformerCompletable())
                .doOnSubscribe { Timber.i("start") }
                .subscribeTag(SIGN_IN_TAG, onComplete = this::onSuccessSignIn, onError = this::onAnyError)
    }

    fun verifyCodeAndSignUp(code: String) {
        interactor.verifyCodeAndSignIn(code).compose(schedulers.getIOToMainTransformerCompletable())
                .doOnSubscribe { Timber.i("start code") }
                .subscribeTag(SIGN_IN_TAG, onComplete = this::onSuccessSignIn, onError = this::onAnyError)
    }

    fun onSuccessSignIn() = ifViewAttached { it.updateProgress(false) }.apply { Timber.i("finish") }
    fun onAnyError(throwable: Throwable) = ifViewAttached { it.updateProgress(false) }.apply { Timber.i("error: $throwable") }
    fun test() {
        navigation.push(SignUpController())
    }
}