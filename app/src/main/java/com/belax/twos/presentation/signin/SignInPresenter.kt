package com.belax.twos.presentation.signin

import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.BaseInput
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.business.AuthInteractor
import com.belax.twos.logExeption
import com.belax.twos.presentation.signup.SignUpController
import io.reactivex.Single

class SignInPresenter(
        private val interactor: AuthInteractor,
        navigation: Navigation,
        schedulers: RxSchedulers
) : RxMvpBasePresenter<SignInView, SignInState, BaseInput>(schedulers, navigation) {

    companion object {
        val SIGN_IN_TAG = "SIGN_IN"
    }

    fun signIn(login: String, password: String) {
        testChain().compose(schedulers.getIOToMainTransformerSingle())
                .progressTaskWithSubscribe({
                    logExeption("task finished: ${vs.progress}")
                }).watchPresenter()

    }

    override fun onRestoreView(isRetained: Boolean, input: BaseInput?) {
        super.onRestoreView(isRetained, input)
        logExeption("SignInPresenter onRestoreView = $vs")
        ifViewAttached {
            if (vs.progress) it.inProgress() else it.outProgress()
        }
    }

    //todo вынести retain флаг в state
    private fun testChain() = Single.fromCallable {
        logExeption("task started: ${vs.progress}")
        Thread.sleep(3000)
        return@fromCallable true
    }

    fun goSignOut() { navigation.push(SignUpController()) }
}

