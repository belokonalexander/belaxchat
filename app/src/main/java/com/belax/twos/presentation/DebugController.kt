package com.belax.twos.presentation

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.view.ViewGroup
import android.widget.Button
import com.belax.twos.R
import com.belax.twos.arch.BaseMvpViewStateController
import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.base.BaseInput
import com.belax.twos.base.BaseView
import com.belax.twos.base.navigation.TabbedNavigation
import com.belax.twos.PagerActivity
import com.belax.twos.logExeption
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

class DebugController : BaseMvpViewStateController<DebugView, DebugPresenter, DebugState, BaseInput>(), DebugView {
    override fun createState() = DebugState()
    override fun getTitle(): String = "Test"
    override fun getLayoutRes(): Int = R.layout.layout_debug
    override fun createPresenter(): DebugPresenter = component().getHomeComponent(getMainRouter(), getInnerRouter()).getDebugPresenter()

    override fun showDebugPanel(actions: List<Action>) {
        view?.let { view ->
            actions.forEach { action ->
                (view as ViewGroup).addView(Button(applicationContext).apply {
                    text = action.title
                    setOnClickListener {
                        action.doAction()
                    }
                })
            }
        }
    }
}

interface DebugView: BaseView {
    fun showDebugPanel(actions: List<Action>)
}

@Parcelize
class DebugState(var actions: List<Action>? = null) : Parcelable

@Parcelize
open class Action(val type: DebugType, val title: String = type.name) : Parcelable {
    @IgnoredOnParcel var presenter: DebugPresenter? = null
    fun doAction() {
        presenter?.let { type.action(it) }
    }
}

enum class DebugType(val action: (DebugPresenter) -> Unit) {
    PAGER({ it.goToPager() }),
    OTHER({ it.goToPager() })
}

class DebugPresenter(private val context: Context, navigation: TabbedNavigation) : RxMvpBasePresenter<DebugView, DebugState, BaseInput>(navigation = navigation) {

    private val actions: List<Action> by lazy {
        mutableListOf<Action>().apply {
            DebugType.values().forEach { type ->
                add(Action(type))
            }
        }
    }

    override fun onFirstAttach() {
        super.onFirstAttach()
        if (vs.actions == null) {
            vs.actions = actions
            showActions()
        }
    }

    override fun onRestoreView(isRetained: Boolean, input: BaseInput?) {
        super.onRestoreView(isRetained, input)
        showActions()
    }

    private fun showActions() {
        vs.actions?.let { actions ->
            actions.forEach { it.presenter = this }
            ifViewAttached { it.showDebugPanel(actions) }
        }
    }

    fun goToPager() {
        val intent = Intent(context, PagerActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        context.startActivity(intent)
    }
}
