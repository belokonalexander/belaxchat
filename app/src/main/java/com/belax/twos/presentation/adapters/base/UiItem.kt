package com.belax.twos.presentation.adapters.base

/**
 * Created by Alexander on 31.01.2018.
 */
interface UiItem {
    val id: Int
}