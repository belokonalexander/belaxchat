package com.belax.twos.presentation.gallery

import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.BaseInput
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.base.navigation.TabbedNavigation
import com.belax.twos.business.GalleryInteractor
import com.belax.twos.logExeption
import com.belax.twos.presentation.camera.CameraController
import io.reactivex.Observable

/**
 * Created by Alexander on 18.03.2018.
 */
class GalleryPresenter(
        private val interactor: GalleryInteractor,
        rxSchedulers: RxSchedulers,
        navigation: Navigation
) : RxMvpBasePresenter<GalleryView, GalleryState, BaseInput>(rxSchedulers, navigation) {

    override fun onAttachAction() {
        super.onAttachAction()
        interactor.getScrapbooks()
                .flatMap { Observable.fromIterable(it).map (mapper.toScrapbookUi()).toList() }
                .compose(schedulers.getIOToMainTransformerSingle())
                .subscribe { data ->
                    if (data.isNotEmpty()) {
                        if (vs.items != data) {
                            vs.items = data
                            ifViewAttached { it.setItems(data) }
                        }
                    } else {
                        logExeption("empty")
                    }
                    ifViewAttached { it.setLastUpdated(data.firstOrNull()) }
                }.watchPresenter()
    }

    override fun onRestoreView(isRetained: Boolean, input: BaseInput?) {
        super.onRestoreView(isRetained, input)
        ifViewAttached { view ->
            view.setItems(vs.items)
            vs.items.firstOrNull()?.let {
                view.setLastUpdated(it)
            }
        }
    }

    fun updateSelectedProject(scrapbookId: Int?) {
        (navigation as TabbedNavigation).pushMain(CameraController(), id = scrapbookId)
    }

    fun createScrapbook() {
        (navigation as TabbedNavigation).pushMain(CameraController())
    }
}