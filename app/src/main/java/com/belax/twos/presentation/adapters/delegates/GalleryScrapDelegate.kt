package com.belax.twos.presentation.adapters.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.belax.twos.R
import com.belax.twos.base.glide.GlideApp
import com.belax.twos.business.entities.ScrapUiModel
import com.belax.twos.logExeption
import com.belax.twos.presentation.adapters.base.BaseDelegate
import com.belax.twos.presentation.adapters.base.BaseHolder
import kotlinx.android.synthetic.main.item_gallery_scrap.*

class GalleryScrapDelegate(private val onClickCallback: (ScrapUiModel) -> Unit) : BaseDelegate<ScrapUiModel, GalleryScrapDelegate.ScrapHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): ScrapHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_gallery_scrap, parent, false)
        return ScrapHolder(view).apply {
            itemView.setOnClickListener { item?.let { onClickCallback(it) } }
        }
    }

    class ScrapHolder(view: View) : BaseHolder<ScrapUiModel>(view) {
        override fun bindTo(item: ScrapUiModel) {
            super.bindTo(item)
            logExeption("width: ${itemView.width}")
            GlideApp.with(itemView).load(item.picture).into(imagePicture )
        }
    }
}

