package com.belax.twos.presentation.adapters.delegates

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.belax.twos.logExeption
import com.belax.twos.presentation.adapters.base.BaseDelegate
import com.belax.twos.presentation.adapters.base.BaseHolder
import com.belax.twos.presentation.adapters.base.UiItem

class StringDelegate : BaseDelegate<StringUi, StringDelegate.StringHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): StringHolder = StringHolder(TextView(parent.context)).apply {
        itemView.setOnClickListener { logExeption("#1021 click: ${item?.title}") }
    }

    class StringHolder(view: View) : BaseHolder<StringUi>(view) {
        override fun bindTo(item: StringUi) {
            super.bindTo(item)
            (itemView as TextView).text = item.title
        }
    }
}

class StringUi(val title: String) : UiItem {
    override val id = title.hashCode()
}