package com.belax.twos.presentation.preview

import android.os.Bundle
import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.business.PreviewInteractor
import com.belax.twos.business.entities.ScrapUiModel
import com.belax.twos.data.PermissionStatus
import com.belax.twos.delayedAction
import com.belax.twos.logExeption
import com.belax.twos.presentation.camera.CameraPresenter.Companion.SCRAPBOOK_ID
import com.belax.twos.presentation.camera.CameraPresenter.Companion.SCRAP_RESULT

/**
 * Created by Alexander on 08.02.2018.
 */
class PreviewPresenter(
        private val interactor: PreviewInteractor,
        rxSchedulers: RxSchedulers,
        navigation: Navigation
) : RxMvpBasePresenter<PreviewView, PreviewState, PreviewViewInput>(rxSchedulers, navigation) {

    override fun onAttachAction() {
        super.onAttachAction()
        logExeption("vs = ${vs.scrapbookId} / ${vs.stringSource}", "#1027")
        vs.stringSource?.let {
            interactor.getPreviewImage(it)
                    .compose(schedulers.getIOToMainTransformerSingle())
                    .subscribe({ picture ->
                        ifViewAttached { it.showPicture(picture) }
                    }, { onUnexpectedError() }) //todo if there can be other errors
                    .watchPresenter()
        } ?: onUnexpectedError()
    }

    override fun mergeStateWithArgs(input: PreviewViewInput) {
        super.mergeStateWithArgs(input)
        input.scrapbookId?.let { vs.scrapbookId = it }
        input.stringSource?.let { vs.stringSource = it }
    }

    private fun returnToCamera(resultScrap: ScrapUiModel? = null) {
        val result = Bundle().apply {
            resultScrap?.let {
                putInt(SCRAPBOOK_ID, it.scrapbookId)
                putParcelable(SCRAP_RESULT, it)
            }
        }
        delayedAction(500, schedulers.getMainThreadScheduler(), { navigation.goBack(result) })
    }

    fun savePicture() {
        val scrapbookId = vs.scrapbookId
        val key = vs.stringSource
        interactor.getStoragePermissionStatus().subscribe { status ->
            key?.let { key ->
                when (status) {
                    PermissionStatus.ALLOWED -> saveToStorage(scrapbookId, key)
                    else -> interactor.requestStoragePermission()
                            .compose(schedulers.getIOToMainTransformerSingle())
                            .subscribeTag("STORAGE", TaskType.START_NEW, {
                                if (it == PermissionStatus.ALLOWED) saveToStorage(scrapbookId, key) else {
                                }
                            })

                }
            } ?: onUnexpectedError()
        }
    }

    private fun onUnexpectedError() {
        logExeption("error vs = ${vs.scrapbookId} / ${vs.stringSource}", "#1027")
        returnToCamera()
    }

    private fun saveToStorage(scrapbookId: Int?, key: String) {
        interactor.addItemToProject(scrapbookId, key)
                .compose(schedulers.getIOToMainTransformerSingle())
                .map(mapper.toScrapUi())
                .subscribeTag("PICTURE", TaskType.IGNORE, { returnToCamera(it) }, ::onError)
    }
}