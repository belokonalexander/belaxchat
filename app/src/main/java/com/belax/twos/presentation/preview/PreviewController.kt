package com.belax.twos.presentation.preview

import android.annotation.SuppressLint
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.belax.twos.R
import com.belax.twos.arch.BaseMvpViewStateController
import com.belax.twos.base.BaseInput
import com.belax.twos.base.BaseView
import com.belax.twos.base.glide.GlideApp
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.layout_preview.view.*

class PreviewController : BaseMvpViewStateController<PreviewView, PreviewPresenter, PreviewState, PreviewViewInput>(), PreviewView {
    override fun getTitle(): String = "Preview"
    override fun getLayoutRes(): Int = R.layout.layout_preview
    override fun createPresenter(): PreviewPresenter = component().getRetainComponent(getMainRouter()).getPreviewPresenter()
    override fun createState(): PreviewState = PreviewState()
    override fun getInput() = PreviewViewInput(pollBundleId(), pollBundleKey())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        return super.onCreateView(inflater, container).apply {
            buttonConfirm.setOnClickListener { presenter.savePicture() }
        }
    }

    override fun showPicture(byteArray: ByteArray) {
        view?.imagePreview?.let {
            GlideApp.with(it).load(byteArray).into(it)
        }
    }
}

@SuppressLint("ParcelCreator")
@Parcelize
class PreviewState(var stringSource: String? = null, var scrapbookId: Int? = null) :  Parcelable

data class PreviewViewInput(var scrapbookId: Int?, var stringSource: String?) : BaseInput()

interface PreviewView : BaseView {
    fun showPicture(byteArray: ByteArray)
}