package com.belax.twos.presentation.camera

import android.os.Bundle
import com.belax.twos.cachedSingle
import com.belax.twos.arch.RxMvpBasePresenter
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.business.CameraInteractor
import com.belax.twos.business.entities.CameraSettings
import com.belax.twos.business.entities.ScrapUiModel
import com.belax.twos.business.entities.ScrapbookUiModel
import com.belax.twos.data.PermissionStatus
import com.belax.twos.log
import com.belax.twos.logExeption
import com.belax.twos.presentation.preview.PreviewController
import com.belax.twos.presentation.viewer.ViewerController

/**
 * Created by Alexander on 08.02.2018.
 */
class CameraPresenter(
        private val interactor: CameraInteractor,
        rxSchedulers: RxSchedulers,
        navigation: Navigation
) : RxMvpBasePresenter<CameraView, CameraState, CameraViewInput>(rxSchedulers, navigation) {

    companion object {
        const val permissionRequestTag = "PERMISSION"
        const val SCRAPBOOK_ID = "SCRAPBOOK_ID"
        const val SCRAP_RESULT = "SCRAP_RESULT"
    }

    override fun onAttachAction() {
        if (!isTaskInWorkState(permissionRequestTag)) {
            startCamera()
        }
    }

    override fun mergeStateWithArgs(input: CameraViewInput) {
        super.mergeStateWithArgs(input)
        input.scrapbookId?.let {
            if (vs.scrapbookId != it) {
                vs.scrapbookId = it
                vs.cameraSettings = vs.cameraSettings?.copy(scrapbookId = it)
            }
        }
        input.scrapResult?.let {
            vs.scrapbookUiModel?.apply { scraps = (listOf(it) + scraps).toSet() }
        }
    }

    override fun onRestoreView(isRetained: Boolean, input: CameraViewInput?) {
        super.onRestoreView(isRetained, input)
        ifViewAttached { view ->
            vs.cameraSettings?.let { view.showCameraContent(it) }
            vs.scrapbookUiModel?.let {
                view.showGalleryPreview(getHoodScrap(it, vs.cameraSettings))
                setGalleryItems(view, it)
            }
            when (vs.bottomSheetOpened) {
                true -> ifViewAttached { it.openBottomSheet() }
                else -> ifViewAttached { it.closeBottomSheet() }
            }
        }
    }

    //todo move to interactor?
    private fun getHoodScrap(scrapbook: ScrapbookUiModel, cameraSettings: CameraSettings?): ScrapUiModel? {
        if (cameraSettings == null) {
            return scrapbook.scraps.firstOrNull()
        } else {
            return scrapbook.scraps.find { it.id == cameraSettings.selectedScrapId } ?: scrapbook.scraps.firstOrNull()
        }
    }

    private fun startCamera() {
        interactor.getCameraPermissionStatus().subscribe { status ->
            when (status) {
                PermissionStatus.ALLOWED -> permissionsCameraGranted()
                else -> interactor.requestCameraPermission().compose(schedulers.getIOToMainTransformerSingle())
                        .subscribeTag(permissionRequestTag, TaskType.IGNORE, {
                            if (it == PermissionStatus.ALLOWED) permissionsCameraGranted() else permissionsCameraDenied()
                        })
            }
        }
    }

    private fun permissionsCameraGranted() {
        cachedSingle(vs.cameraSettings, interactor.getCameraSettings(vs.scrapbookUiModel?.id))
                .compose(schedulers.getIOToMainTransformerSingle())
                .subscribe({ settings ->
                    showCameraContent(settings)
                    showScrapbookData()
                }, ::onError).watchPresenter()
    }

    private fun showScrapbookData() {
        vs.scrapbookId?.let {
            interactor.getScrapbook(it).compose(schedulers.getIOToMainTransformerSingle())
                    .map (mapper.toScrapbookUi())
                    .subscribe({ scrapbook ->
                        //update if repo returns different state
                        if (vs.scrapbookUiModel != scrapbook) {
                            vs.scrapbookUiModel = scrapbook
                            ifViewAttached { it.showGalleryPreview(getHoodScrap(scrapbook, vs.cameraSettings)) }
                            ifViewAttached { setGalleryItems(it, scrapbook) }
                        }
                    }, {
                        //todo data not found
                    })
        } ?:  ifViewAttached { it.showGalleryPreview(null) }
    }

    private fun setGalleryItems(view: CameraView, scrapbook: ScrapbookUiModel) {
        view.setGalleryItems(scrapbook.scraps.toList())
    }

    private fun permissionsCameraDenied() {
        //todo
    }

    fun permissionsStorageDenied() {
        //todo
    }

    private fun pictureTaken(picture: ByteArray, cameraOrientation: Int, cameraSettings: CameraSettings, scrapbookId: Int?) {
        log("taken picture ${picture.size} / orientation: ${cameraOrientation}", "#1028")
        if (picture.isEmpty()) {
            return
        }

        interactor.preparePicture(picture, cameraOrientation, cameraSettings)
                .compose(schedulers.getIOToMainTransformerSingle())
                .progressTaskWithSubscribe ({ result ->
                    navigation.push(PreviewController(), key = result, id = scrapbookId)
                })
    }

    fun takePicture() {
        ifViewAttached {
            it.takePicture { data, orientation, cameraSettings -> pictureTaken(data, orientation, cameraSettings, vs.scrapbookId) }
        }
    }

    fun changeCamera(cameraSettings: CameraSettings) {
        showCameraContent(cameraSettings.copy(cameraType = cameraSettings.cameraType.alternative()))
    }

    private fun showCameraContent(settings: CameraSettings) {
        vs.cameraSettings = settings
        ifViewAttached {
            it.showCameraContent(settings)
        }
    }

    fun closeBottomSheet() {
        if (vs.bottomSheetOpened) {
            vs.bottomSheetOpened = false
            ifViewAttached { it.closeBottomSheet() }
        }
    }

    fun openBottomSheet() {
        if (!vs.bottomSheetOpened) {
            vs.bottomSheetOpened = true
            ifViewAttached { it.openBottomSheet() }
        }
    }

    override fun handleBack(): Boolean {
        if (vs.bottomSheetOpened) {
            closeBottomSheet()
            return true
        }
        return super.handleBack()
    }

    fun scrapGallerySelected(scrap: ScrapUiModel) {
        navigation.push(ViewerController(), parcelable = vs.scrapbookUiModel, id = scrap.id)
    }

}