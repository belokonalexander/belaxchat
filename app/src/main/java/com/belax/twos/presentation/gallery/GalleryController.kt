package com.belax.twos.presentation.gallery

import android.annotation.SuppressLint
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.belax.twos.R
import com.belax.twos.arch.BaseMvpViewStateController
import com.belax.twos.base.BaseInput
import com.belax.twos.base.BaseView
import com.belax.twos.business.entities.ScrapbookUiModel
import com.belax.twos.presentation.adapters.base.BaseAdapter
import com.belax.twos.presentation.adapters.delegates.GalleryScrapbookDelegate
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.layout_gallery.view.*
import javax.inject.Inject

/**
 * Created by Alexander on 18.03.2018.
 */
class GalleryController : BaseMvpViewStateController<GalleryView, GalleryPresenter, GalleryState, BaseInput>(), GalleryView {

    private var adapter: BaseAdapter? = null

    override fun getTitle(): String = "Gallery"
    override fun getLayoutRes(): Int = R.layout.layout_gallery
    override fun createPresenter(): GalleryPresenter = component().getHomeComponent(getMainRouter(), getInnerRouter()).getGalleryPresenter()
    override fun createState(): GalleryState = GalleryState(emptyList())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        component().getHomeComponent(getMainRouter(), getInnerRouter()).inject(this)
        adapter = BaseAdapter(listOf(createGalleryDelegate()))
        return super.onCreateView(inflater, container).apply {
            fab.setOnClickListener { presenter.createScrapbook() }
        }
    }

    private fun createGalleryDelegate() = GalleryScrapbookDelegate({ presenter.updateSelectedProject(it.id) })

    override fun onAttach(view: View) {
        super.onAttach(view)
        view.recyclerGallery.let {
            it.adapter = adapter
            it.layoutManager = LinearLayoutManager(applicationContext)
        }
    }

    override fun setLastUpdated(scrapbookUiModel: ScrapbookUiModel?) {
        view?.viewLatest?.bind(scrapbookUiModel, ::onUpdateLatestClick)
    }

    override fun setItems(items: List<ScrapbookUiModel>) {
        adapter?.let {
            it.items = items
            it.notifyDataSetChanged()
        }
    }

    fun onUpdateLatestClick(item: ScrapbookUiModel?) {
        presenter.updateSelectedProject(item?.id)
    }

    override fun onDestroyView(view: View) {
        super.onDestroyView(view)
        adapter = null
    }
}

@SuppressLint("ParcelCreator")
@Parcelize
class GalleryState(var items: List<ScrapbookUiModel>) : Parcelable

interface GalleryView : BaseView {
    fun setItems(items: List<ScrapbookUiModel>)
    fun setLastUpdated(scrapbookUiModel: ScrapbookUiModel?)
}