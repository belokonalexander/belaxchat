package com.belax.twos.presentation.adapters.base

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate

abstract class BaseDelegate<T, B : BaseHolder<T>> : AdapterDelegate<AdapterDataType>() {

    override fun isForViewType(items: AdapterDataType, position: Int): Boolean = toBindingItem(items, position) != null

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(items: AdapterDataType, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as B).bindTo(toBindingItem(items, position)!!)
    }

    @Suppress("UNCHECKED_CAST")
    private fun toBindingItem(items: AdapterDataType, position: Int): T? = items[position] as? T

    abstract override fun onCreateViewHolder(parent: ViewGroup): B

}
