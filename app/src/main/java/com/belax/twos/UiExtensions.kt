package com.belax.twos

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.Surface
import android.view.View
import android.view.WindowManager
import com.belax.twos.views.styles.LollipopRippleDrawable
import com.belax.twos.views.styles.PreLollipopRippleDrawable


/**
 * Created by Alexander on 04.03.2018.
 */
fun Resources.dpToPx(dp: Int) = ((dp * displayMetrics.density) + 0.5f).toInt()
fun Resources.pxToDp(px: Int) = ((px / displayMetrics.density) + 0.5f).toInt()
fun Resources.spToPx(sp: Int) = ((sp * displayMetrics.scaledDensity) + 0.5f).toInt()
fun Resources.dimensPx(resource: Int) = getDimension(resource).toInt()
fun Resources.width() = displayMetrics.widthPixels
fun Resources.height() = displayMetrics.heightPixels
fun Resources.isPortrait() = height() > width()
fun Resources.isLandscape() = width() > height()
fun Context.rotationAngle() = (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.rotation.let {
        when (it) {
                Surface.ROTATION_0 -> 0
                Surface.ROTATION_90 -> 90
                Surface.ROTATION_180 -> 180
                else -> 270
        }
}
fun Resources.density() = displayMetrics.density
fun getRippleDrawable(normalColor: Int, pressedColor: Int): Drawable = when (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        true -> LollipopRippleDrawable(normalColor, pressedColor).getDrawable()
        else -> PreLollipopRippleDrawable(normalColor, pressedColor).getDrawable()
}

fun View.setVisible(visible: Boolean) {
        if (visible) {
                visibility = View.VISIBLE
        } else {
                visibility = View.GONE
        }
}

fun View.dpToPx(dp: Int) = resources.dpToPx(dp)
fun View.spToPx(sp: Int) = resources.dpToPx(sp)
fun View.dimensPx(resource: Int) = resources.dimensPx(resource)