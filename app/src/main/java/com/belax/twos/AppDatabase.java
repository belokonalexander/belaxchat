package com.belax.twos;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.belax.twos.data.database.Converters;
import com.belax.twos.data.database.Scrap;
import com.belax.twos.data.database.Scrapbook;
import com.belax.twos.data.database.dao.ScrapDao;
import com.belax.twos.data.database.dao.ScrapbookDao;

@Database(version = 3, entities = {Scrapbook.class, Scrap.class})
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract ScrapbookDao scrapbookDao();
    public abstract ScrapDao scrapDao();
}
