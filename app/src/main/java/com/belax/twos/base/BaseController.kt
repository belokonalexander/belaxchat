package com.belax.twos.base

import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.Toast
import com.belax.twos.App
import com.belax.twos.arch.FullRestorableViewState
import com.belax.twos.base.navigation.NavigationImpl.Companion.BUNDLE_ID
import com.belax.twos.base.navigation.NavigationImpl.Companion.BUNDLE_KEY
import com.belax.twos.base.navigation.NavigationImpl.Companion.BUNDLE_RESULT
import com.belax.twos.base.navigation.NavigationImpl.Companion.INPUT_PARCELABLE
import com.belax.twos.log
import com.belax.twos.logExeption
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.hannesdorfmann.mosby3.conductor.viewstate.MvpViewStateController
import kotlinx.android.synthetic.main.toolbar.view.*

abstract class BaseController<V : BaseView, P : BasePresenter<V, S, I>, VS : FullRestorableViewState<S, V>, S : Parcelable, I : BaseInput>()
    : MvpViewStateController<V, P, VS>(), BaseView {

    fun pollBundleId(): Int? = args.getInt(BUNDLE_ID, -1).takeIf { it != -1 }?.also { args.remove(BUNDLE_ID) }
    fun pollBundleKey(): String? = args.getString(BUNDLE_KEY)?.also { args.remove(BUNDLE_KEY) }
    fun pollBundleResult(): Bundle? = args.getBundle(BUNDLE_RESULT)?.also { args.remove(BUNDLE_RESULT) }
    fun <T : Parcelable?> pollBundleParcelable(): T? = args.getParcelable<T>(INPUT_PARCELABLE)?.also { args.remove(INPUT_PARCELABLE) }

    protected var toolbar: Toolbar? = null
        private set

    override fun initializeView(stackSize: Int) {
        toolbar?.apply {
            if (stackSize > 1) {
                navigationIcon = resources!!.getDrawable(android.R.drawable.arrow_down_float)
                setNavigationOnClickListener { router.handleBack() }
            }
        }
    }

    protected fun component() = App.get(applicationContext!!).component()
    protected fun activity(): BaseActivity = activity as BaseActivity

    private fun setTitle() {
        toolbar?.apply { title = this@BaseController.getTitle() }
    }

    override fun showError(error: Throwable) {
        Toast.makeText(applicationContext, error.localizedMessage, Toast.LENGTH_LONG).show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        log("onCreateView[${this.javaClass.simpleName}]")
        return createViewFromCache() ?: inflater.inflate(getLayoutRes(), container, false)
    }

    override fun setViewState(viewState: VS) {
        super.setViewState(viewState)
        provideViewState()?.let {
            presenter.attachViewState(it)
        } ?: throw RuntimeException("Something wrong with provideViewState() method")

    }

    override fun onViewStateInstanceRestored(instanceStateRetained: Boolean) {
        presenter.onRestoreView(instanceStateRetained, getInput())

    }

    override fun onNewViewStateInstance() {
        presenter.onRestoreView(false, getInput())
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        log("onAttach[${this.javaClass.simpleName}]")
        toolbar = view.toolbar
        setTitle()
        presenter.onAttachAction()
    }

    private fun provideViewState() = ((viewState as FullRestorableViewState<*, *>).state as? S)

    override fun onDetach(view: View) {
        super.onDetach(view)
        log("onDetach[${this.javaClass.simpleName}]")
    }

    override fun onDestroyView(view: View) {
        super.onDestroyView(view)
        toolbar = null
        log("onDestroyView[${this.javaClass.simpleName}]")
    }

    override fun onDestroy() {
        super.onDestroy()
        log("onDestroy[${this.javaClass.simpleName}]")
    }

    override fun inProgress() {}
    override fun outProgress() {}

    fun getMainRouter(): Router {
        var topController: Controller = this
        while (topController.parentController != null) {
            topController = topController.parentController!!
        }
        return topController.router
    }

    override fun handleBack(): Boolean {
        return presenter.handleBack() || super.handleBack()
    }

    //todo improve if we will more than one child router
    fun getInnerRouter(): Router = childRouters.firstOrNull() ?: router

    open fun createViewFromCache(): View? = null

    abstract fun getTitle(): String
    abstract fun getLayoutRes(): Int
    open fun getInput(): I? = null
}

