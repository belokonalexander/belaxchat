package com.belax.twos.base

import com.belax.twos.business.entities.ScrapUiModel
import com.belax.twos.business.entities.ScrapbookUiModel
import com.belax.twos.data.database.Scrap
import com.belax.twos.data.database.ScrapbookAndScraps
import io.reactivex.functions.Function

class RxMapper {

    fun toScrapbookUi(): Function<ScrapbookAndScraps, ScrapbookUiModel> = Function {
        return@Function ScrapbookUiModel(
                it.scrapbook.id,
                it.scrapbook.title,
                it.scrapbook.description,
                it.scraps.map { toScrapUi().apply(it) }.toSet(),
                it.scrapbook.created
        )
    }

    fun toScrapUi(): Function<Scrap, ScrapUiModel> = Function {
        return@Function ScrapUiModel(it.id, it.title, it.picture, it.scrapbookId, it.created)
    }
}