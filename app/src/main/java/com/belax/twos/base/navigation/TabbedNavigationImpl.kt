package com.belax.twos.base.navigation

import android.os.Bundle
import android.os.Parcelable
import com.belax.twos.di.retain.TabFactory
import com.belax.twos.log
import com.belax.twos.views.BottomNavigationView
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class TabbedNavigationImpl(
        private val mainNavigation: Navigation,
        private val innerNavigation: Navigation,
        private val tabFactory: TabFactory
) : TabbedNavigation {

    companion object {
        fun getTagValue(controller: Controller) = controller.args.getString("TAG") ?: controller.instanceId
        fun isRetainedController(controller: Controller) = controller.args.getBoolean("RETAIN")
        fun isRoot(controller: Controller) = controller.args.getBoolean("ROOT")
        fun isHome(controller: Controller) = controller.args.getBoolean("HOME")
    }

    private val tabViewCommandsSubject = PublishSubject.create<TabViewCommand>()
    private val tabViewCommandsChanges = tabViewCommandsSubject as Observable<TabViewCommand>

    override fun switchTab(target: BottomNavigationView.ItemMeta) {
        val controller = tabFactory(target)
        switchTab(controller)
    }

    override fun getTabViewChanges(): Observable<TabViewCommand> {
        return tabViewCommandsChanges
    }

    private fun switchTab(controller: Controller) {
        val lastRootTransaction = getCurrentRootTransaction()

        if (!isRoot(controller)) throw RuntimeException("Only root controllers allowed")

        // ignore the same controllers
        if (lastRootTransaction != null && getTagValue(lastRootTransaction.controller()) == getTagValue(controller))
            return

        if (innerNavigation.getRouter().getControllerWithTag(getTagValue(controller)) == null) {
            if (isRoot(controller) && lastRootTransaction != null && getRootTag(lastRootTransaction) != getTagValue(controller)) {
                val targetBackstack = innerBackstack().filter { filterBackstack(lastRootTransaction, it) }.toMutableList().apply {
                    add(RouterTransaction.with(controller).tag(getTagValue(controller)))
                }
                innerNavigation.getRouter().setBackstack(targetBackstack, null)
            } else innerNavigation.push(controller, getTagValue(controller))
        } else {
            val targetStack = innerBackstack()
                    .filter { filterBackstack(lastRootTransaction, it) }
                    .sortedBy { getTagValue(controller) == getRootTag(it) }
            innerNavigation.getRouter().setBackstack(targetStack, null)
        }
        printStack()
    }

    private fun innerBackstack() = innerNavigation.getRouter().backstack

    /**
     * when true then transaction will stay in backstack
     * false - transaction should be removed
     */
    private fun filterBackstack(lastRootTransaction: RouterTransaction?, transaction: RouterTransaction): Boolean {
        if (lastRootTransaction != null && !isRetainedController(lastRootTransaction.controller())) {
            //if transaction is a part of last retain transaction - delete it
            return getRootTag(lastRootTransaction) != getRootTag(transaction)
        }
        return true
    }

    override fun clearCurrentTab() {
        getCurrentRootTransaction()?.controller()?.let { rootController ->
            val rootTag = getTagValue(rootController)
            val targetStack = innerBackstack()
                    .filter { getRootTag(it) != rootTag || getTagValue(it.controller()) == rootTag }
            innerNavigation.getRouter().setBackstack(targetStack, null)
        }
    }

    override fun getStackSize(): Int {
        val lastRootTag = getRootTag(innerNavigation.getRouter().backstack.last())
        return innerBackstack().count { getRootTag(it) == lastRootTag }
    }

    private fun printStack() {
        innerBackstack().joinToString { it.tag() ?: "empty" }.let { log(it, "#1127") }
    }

    /**
     * returns result tag for child in root tab
     */
    private fun getChildStringTag(parentTag: String, childTag: String): String = parentTag.plus(wrapChildTag(childTag))

    /**
     * returns root tab tag from result child tag
     */
    private fun getRootTag(transaction: RouterTransaction): String = transaction.tag()!!.substringBefore(wrapChildTag(getTagValue(transaction.controller())))

    /**
     * returns child tag second part in special delimiter
     */
    private fun wrapChildTag(childTag: String): String = "[$childTag]"

    /**
     * returns tag with `rootController[childController]` format
     */
    private fun generateTag(controller: Controller): String? = getCurrentRootTransaction()?.let {
        getChildStringTag(getTagValue(it.controller()), getTagValue(controller))
    }

    private fun getTopController(): Controller? = innerBackstack().lastOrNull()?.controller()

    private fun getCurrentRootTransaction(): RouterTransaction? = innerBackstack().lastOrNull { isRoot(it.controller()) }

    override fun push(controller: Controller, tag: String?, id: Int?, key: String?, parcelable: Parcelable?) {
        val targetTag = if (isRoot(controller)) tag else generateTag(controller) ?: throw RuntimeException("Added controller without right tag")
        innerNavigation.push(controller, targetTag)
        printStack()
    }

    override fun goBack(result: Bundle?): Boolean {
        val currentController = getTopController()
        //todo there is bug when is tab root controller and we try to handle back by local (controller) handler
        if (currentController != null && isRoot(currentController)) {
            if (!isHome(currentController)) {
                innerBackstack().map { it.controller() }.firstOrNull { isHome(it) }?.let {
                    switchTab(it)
                    tabViewCommandsSubject.onNext(TabViewCommand.RESET)
                    return true
                }
            }
            //we can handle only if is not root of Application. Otherwise return false and delegate to router creator
            return if (mainNavigation.getStackSize() > 1) mainNavigation.getRouter().popCurrentController() else false
        }
        return innerNavigation.goBack(result)
    }

    override fun getRouter(): Router = innerNavigation.getRouter()

    override fun pushMain(controller: Controller, id: Int?, key: String?) {
        mainNavigation.push(controller, id = id, key = key)
    }
}