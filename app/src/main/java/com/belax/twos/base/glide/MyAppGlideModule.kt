package com.belax.twos.base.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by Alexander on 25.03.2018.
 */
@GlideModule
class MyAppGlideModule : AppGlideModule() {

}