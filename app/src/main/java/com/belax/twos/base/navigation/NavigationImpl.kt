package com.belax.twos.base.navigation

import android.os.Bundle
import android.os.Parcelable
import com.belax.twos.log
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler

class NavigationImpl(private val router: Router) : Navigation {

    override fun getStackSize(): Int = router.backstackSize

    override fun getRouter(): Router = router

    companion object {
        const val BUNDLE_ID = "_bundle_id"
        const val BUNDLE_KEY = "_bundle_key"
        const val INPUT_PARCELABLE = "_bundle_parcelable"
        const val BUNDLE_RESULT = "_bundle_result"
    }

    private val defaultChangeHandler = HorizontalChangeHandler(200)

    override fun goBack(result: Bundle?): Boolean {
        clearResult()
        result?.let { addResult(it) }
        return router.handleBack()
    }

    override fun push(controller: Controller, tag: String?, id: Int?, key: String?, parcelable: Parcelable?) {
        clearResult()
        id?.let { controller.args.putInt(BUNDLE_ID, it) }
        key?.let { controller.args.putString(BUNDLE_KEY, it) }
        parcelable?.let { controller.args.putParcelable(INPUT_PARCELABLE, it) }
        router.pushController(RouterTransaction.with(controller).tag(tag)
                .pushChangeHandler(defaultChangeHandler)
                .popChangeHandler(defaultChangeHandler)).logRouting()
    }

    private fun addResult(result: Bundle) {
        router.backstack.getOrNull(router.backstack.lastIndex - 1)?.apply {
            controller().args.putBundle(BUNDLE_RESULT, result)
        }
    }

    private fun clearResult() {
        router.backstack.lastOrNull()?.controller()?.args?.remove(BUNDLE_RESULT)
    }

    private fun Any.logRouting() = log("routing ${router.printStack()}", "#1021")
}
