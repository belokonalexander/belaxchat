package com.belax.twos.base

import android.Manifest
import com.belax.twos.data.PermissionCode
import com.belax.twos.data.PermissionStatus
import com.belax.twos.data.PermissionsRepository
import io.reactivex.Single
import io.reactivex.rxkotlin.toSingle

/**
 * Created by Alexander on 30.03.2018.
 */
interface PermissionInteractor {
    fun getCameraPermissionStatus(): Single<PermissionStatus>
    fun getStoragePermissionStatus(): Single<PermissionStatus>
    fun requestCameraPermission(): Single<PermissionStatus>
    fun requestStoragePermission(): Single<PermissionStatus>
}

open class PermissionInteractorImpl(private val permissionsRepository: PermissionsRepository) : PermissionInteractor {
    override fun requestCameraPermission(): Single<PermissionStatus> {
        return permissionsRepository.requestPermission(arrayOf(Manifest.permission.CAMERA), PermissionCode.CAMERA_REQUEST_CODE)
    }

    override fun requestStoragePermission(): Single<PermissionStatus> {
        return permissionsRepository.requestPermission(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PermissionCode.WRITE_EXTERNAL_REQUEST_CODE)
    }

    override fun getCameraPermissionStatus(): Single<PermissionStatus> = permissionsRepository.getCameraPermission().toSingle()
    override fun getStoragePermissionStatus(): Single<PermissionStatus> = permissionsRepository.getStoragePermission().toSingle()

}