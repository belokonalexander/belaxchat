package com.belax.twos.base

import android.os.Parcelable
import com.hannesdorfmann.mosby3.mvp.MvpView

/**
 * Created by belokon on 10.12.17.
 */
interface BaseView : MvpView {
    fun initializeView(stackSize: Int)
    fun showError(error: Throwable)
    fun inProgress()
    fun outProgress()
}