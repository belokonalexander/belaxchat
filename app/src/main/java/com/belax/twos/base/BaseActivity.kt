package com.belax.twos.base

import android.content.pm.PackageManager
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.belax.twos.App
import com.belax.twos.data.PermissionStatus
import com.belax.twos.logExeption
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


/**
 * Created by Alexander on 30.03.2018.
 */
abstract class BaseActivity : AppCompatActivity() {

    @Inject lateinit var permissionSubject: PublishSubject<Pair<Int, PermissionStatus>>
    val permissionsChanges by lazy { permissionSubject as Observable<Pair<Int, PermissionStatus>> }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.get(this).component().applicationComponent.inject(this)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionSubject.onNext(Pair(requestCode, PermissionStatus.ALLOWED))
            } else {
                permissionSubject.onNext(Pair(requestCode, PermissionStatus.DISABLED))
            }
        }
    }
}