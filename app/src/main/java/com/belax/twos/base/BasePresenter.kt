package com.belax.twos.base

import android.os.Parcelable
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.logExeption
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter

abstract class BasePresenter<V : BaseView, S : Parcelable, Input : BaseInput>(protected val navigation: Navigation) : MvpBasePresenter<V>() {

    protected lateinit var vs: S
    protected var attachCount = 0

    //called before main back handler, which will interrupted if true was returned
    open fun handleBack(): Boolean { return false }

    fun getStackSize(): Int = navigation.getStackSize()

    /**
     * called [2]
     *
     * remember that onDetachView can be called without onDestroyView
     */
    open fun onAttachAction() {
        if (attachCount == 0) {
            onFirstAttach()
        }
        ifViewAttached { it.initializeView(navigation.getStackSize()) }
        attachCount++
    }

    fun onError(error: Throwable) {
        logExeption(error)
        ifViewAttached { view -> view.showError(error)  }
    }

    /**
     * error which can't be handled (data gap or something)
     * more often we should close current view in this case
     */
    fun onCriticalError() { }

    /**
     * runs commands for view for restore right state [1(2)]
     * calls always before attach - VS was created, or restored, or from retain memory
     */
    open fun onRestoreView(isRetained: Boolean, input: Input?) {
        logExeption("onRestoreView")
        input?.let { mergeStateWithArgs(it) }
        attachCount = 0
    }

    /**
     * called on first attach after vs was restored
     */
    open fun onFirstAttach() { }

    /**
     * vs was created or restored, won't call when VS in retain memory
     */
    fun attachViewState(state: S) {
        logExeption("attachViewState")
        vs = state
    }

    /**
     * view state can be not actual after reattach and we should update view state
     * also we can cancel current run tasks with not actual view state data
     */
    protected open fun mergeStateWithArgs(input: Input) {}

}