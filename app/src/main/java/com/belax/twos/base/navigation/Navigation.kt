package com.belax.twos.base.navigation

import android.os.Bundle
import android.os.Parcelable
import com.belax.twos.views.BottomNavigationView
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import io.reactivex.Observable

interface Navigation {
    fun push(controller: Controller, tag: String? = null, id: Int? = null, key: String? = null, parcelable: Parcelable? = null)
    fun goBack(result: Bundle? = null): Boolean
    fun getRouter(): Router
    fun getStackSize(): Int
}

interface TabbedNavigation : Navigation {
    fun pushMain(controller: Controller, id: Int? = null, key: String? = null)
    fun switchTab(target: BottomNavigationView.ItemMeta)
    fun clearCurrentTab()
    fun getTabViewChanges(): Observable<TabViewCommand>
}
enum class TabViewCommand { RESET }
internal fun Router.printStack(): String = backstack.joinToString { it.controller().javaClass.simpleName.plus(it.tag()?.let { "($it)" } ?: "")  }