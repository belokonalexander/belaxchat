package com.belax.twos.base

interface ProgressViewState {
    var progress: Boolean
}