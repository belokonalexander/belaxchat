package com.belax.twos.di.home

import android.content.Context
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.base.navigation.NavigationImpl
import com.belax.twos.base.navigation.TabbedNavigation
import com.belax.twos.base.navigation.TabbedNavigationImpl
import com.belax.twos.business.HomeInteractor
import com.belax.twos.business.HomeInteractorImpl
import com.belax.twos.data.HardwareRepository
import com.belax.twos.di.main.ActivityProvider
import com.belax.twos.di.retain.TabFactory
import com.belax.twos.presentation.DebugPresenter
import com.belax.twos.presentation.home.HomePresenter
import com.bluelinelabs.conductor.Router
import dagger.Module
import dagger.Provides
import javax.inject.Named

@JvmSuppressWildcards
@Module
class HomeModule(private val homeRouter: Router) {

    @HomeScope
    @Provides
    fun provideHomeInteractor(hardwareRepository: HardwareRepository): HomeInteractor = HomeInteractorImpl(hardwareRepository)

    @HomeScope
    @Provides
    fun provideHomePresenter(
            homeInteractor: HomeInteractor,
            rxSchedulers: RxSchedulers,
            tabNavigation: TabbedNavigation
    ): HomePresenter = HomePresenter(homeInteractor, rxSchedulers, tabNavigation)

    @HomeScope
    @Provides
    @Named("Inner")
    fun provideNavigation(): Navigation = NavigationImpl(homeRouter)

    @HomeScope
    @Provides
    fun provideTabNavigation(
            mainNavigation: Navigation,
            @Named("Inner") innerNavigation: Navigation,
            mapper: TabFactory
    ): TabbedNavigation = TabbedNavigationImpl(mainNavigation, innerNavigation, mapper)

    @HomeScope
    @Provides
    fun providePagerPresenter(tabNavigation: TabbedNavigation, context: Context): DebugPresenter = DebugPresenter(context, tabNavigation)
}