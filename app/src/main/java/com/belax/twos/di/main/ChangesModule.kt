package com.belax.twos.di.main

import com.belax.twos.data.PermissionStatus
import dagger.Module
import dagger.Provides
import io.reactivex.subjects.PublishSubject
import javax.inject.Singleton

@Module
class ChangesModule {

    @Singleton
    @Provides
    fun providePermissionSubject(): PublishSubject<Pair<Int, PermissionStatus>> = PublishSubject.create()

}