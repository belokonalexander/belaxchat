package com.belax.twos.di.main

import com.belax.twos.base.BaseActivity

class ActivityProvider(var currentActivity: BaseActivity?) {
    fun update(activity: BaseActivity?) { currentActivity = activity }
}