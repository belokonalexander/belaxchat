package com.belax.twos.di.retain

import com.belax.twos.di.home.HomeComponent
import com.belax.twos.di.home.HomeModule
import com.belax.twos.di.home.ViewerModule
import com.belax.twos.di.main.UiModule
import com.belax.twos.presentation.camera.CameraPresenter
import com.belax.twos.presentation.preview.PreviewPresenter
import com.belax.twos.presentation.viewer.ViewerPresenter
import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class RetainScope

@Subcomponent(modules = arrayOf(RetainModule::class, UiModule::class, ViewerModule::class))
@RetainScope
interface RetainComponent {
    fun plus(homeModule: HomeModule): HomeComponent
    fun getCameraPresenter(): CameraPresenter
    fun getPreviewPresenter(): PreviewPresenter
    fun getViewerPresenter(): ViewerPresenter
}