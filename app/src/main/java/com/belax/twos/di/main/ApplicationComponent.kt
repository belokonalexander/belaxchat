package com.belax.twos.di.main

import com.belax.twos.base.BaseActivity
import com.belax.twos.di.retain.RetainComponent
import com.belax.twos.di.retain.RetainModule
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(
        ApplicationModule::class,
        FirebaseModule::class,
        ApplicationRepositoryModule::class,
        ChangesModule::class))
@Singleton
interface ApplicationComponent {
    fun inject(baseActivity: BaseActivity)
    fun plus(activityModule: RetainModule): RetainComponent
    fun getActivityProvider(): ActivityProvider
}