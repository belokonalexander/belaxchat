package com.belax.twos.di.home

import com.belax.twos.base.navigation.Navigation
import com.belax.twos.business.ViewerInteractor
import com.belax.twos.business.ViewerInteractorImpl
import com.belax.twos.business.usecases.GetScrapbookUseCase
import com.belax.twos.di.retain.RetainScope
import com.belax.twos.presentation.viewer.ViewerPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by Alexander on 18.03.2018.
 */
@Module
class ViewerModule {

    @RetainScope
    @Provides
    fun provideViewerInteractor(
            getScrapbookUseCase: GetScrapbookUseCase
    ): ViewerInteractor = ViewerInteractorImpl(getScrapbookUseCase)

    @RetainScope
    @Provides
    fun provideViewerPresenter(
            interactor: ViewerInteractor,
            navigation: Navigation
    ): ViewerPresenter = ViewerPresenter(interactor, navigation)

}