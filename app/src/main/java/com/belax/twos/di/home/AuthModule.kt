package com.belax.twos.di.home

import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.navigation.TabbedNavigation
import com.belax.twos.business.AuthInteractor
import com.belax.twos.business.AuthInteractorImpl
import com.belax.twos.data.AuthRepository
import com.belax.twos.data.AuthRepositoryImpl
import com.belax.twos.presentation.signin.SignInPresenter
import com.belax.twos.presentation.signup.SignUpPresenter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import dagger.Module
import dagger.Provides

@Module
class AuthModule {

    @HomeScope
    @Provides
    fun provideAuthRepository(firebaseAuth: FirebaseAuth, phoneAuthProvider: PhoneAuthProvider): AuthRepository = AuthRepositoryImpl(firebaseAuth, phoneAuthProvider)

    @HomeScope
    @Provides
    fun provideAuthInteractor(authRepository: AuthRepository): AuthInteractor = AuthInteractorImpl(authRepository)

    @HomeScope
    @Provides
    fun provideSignInPresenter(authInteractor: AuthInteractor, schedulers: RxSchedulers, navigation: TabbedNavigation): SignInPresenter =
            SignInPresenter(authInteractor, navigation, schedulers)

    @HomeScope
    @Provides
    fun provideSignUpPresenter(authInteractor: AuthInteractor, schedulers: RxSchedulers, navigation: TabbedNavigation): SignUpPresenter =
            SignUpPresenter(authInteractor, navigation, schedulers)

}