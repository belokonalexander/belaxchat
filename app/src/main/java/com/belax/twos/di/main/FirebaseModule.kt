package com.belax.twos.di.main

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FirebaseModule() {

    @Singleton
    @Provides
    fun providesFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Singleton
    @Provides
    fun providesPhoneFirebaseAuth(): PhoneAuthProvider = PhoneAuthProvider.getInstance()

}