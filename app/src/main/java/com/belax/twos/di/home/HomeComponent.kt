package com.belax.twos.di.home

import com.belax.twos.di.retain.HomeUiModule
import com.belax.twos.presentation.DebugPresenter
import com.belax.twos.presentation.gallery.GalleryController
import com.belax.twos.presentation.gallery.GalleryPresenter
import com.belax.twos.presentation.home.HomeController
import com.belax.twos.presentation.home.HomePresenter
import com.belax.twos.presentation.signin.SignInPresenter
import com.belax.twos.presentation.signup.SignUpPresenter
import com.belax.twos.presentation.viewer.ViewerPresenter
import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class HomeScope

@Subcomponent(modules = arrayOf(HomeModule::class, AuthModule::class, HomeUiModule::class, GalleryModule::class))
@HomeScope
interface HomeComponent {
    fun inject(homeController: HomeController)
    fun inject(galleryController: GalleryController)
    fun getHomePresenter(): HomePresenter
    fun getSignInPresenter(): SignInPresenter
    fun getSignUpPresenter(): SignUpPresenter
    fun getGalleryPresenter(): GalleryPresenter
    fun getDebugPresenter(): DebugPresenter
}