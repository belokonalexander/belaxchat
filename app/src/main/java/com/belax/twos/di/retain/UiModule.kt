package com.belax.twos.di.retain

import com.belax.twos.BuildConfig
import com.belax.twos.R
import com.belax.twos.di.home.HomeScope
import com.belax.twos.presentation.DebugController
import com.belax.twos.presentation.gallery.GalleryController
import com.belax.twos.presentation.signin.SignInController
import com.belax.twos.views.BottomNavigationView
import com.bluelinelabs.conductor.Controller
import dagger.Module
import dagger.Provides

/**
 * Created by Alexander on 14.03.2018.
 */

// @JvmSuppressWildcards saved from generate <? extends Something> type
//kotlin list List<Controller> will be List<? extends Controller>. We can use @JvmSuppressWildcards or MutableList

typealias TabFactory = (@JvmSuppressWildcards BottomNavigationView.ItemMeta) -> @JvmSuppressWildcards Controller
@JvmSuppressWildcards
@Module
class HomeUiModule {

    @Provides
    fun provideDefaultTabController(
            tabFactory: TabFactory,
            bottomNavSettings: BottomNavigationView.BottomNavSettings
    ): Controller = tabFactory(bottomNavSettings.tabs.find { it.id == bottomNavSettings.defaultId }!!)

    @HomeScope
    @Provides
    fun provideTabMeta() = BottomNavigationView.BottomNavSettings(mutableListOf<BottomNavigationView.ItemMeta>().apply {
        add(BottomNavigationView.ItemMeta(0, "Home", R.drawable.ic_home_black_24dp))
        add(BottomNavigationView.ItemMeta(1, "Some", R.drawable.ic_photo_camera_black_24dp))
        if (BuildConfig.DEBUG) {
            add(BottomNavigationView.ItemMeta(2, "Some2", R.drawable.ic_build_black_24dp))
        }
    }, 2)

    @HomeScope
    @Provides
    fun provideTabMetaToControllerMapper(settings: BottomNavigationView.BottomNavSettings): TabFactory = {
        fun Controller.setRoot() { args.putBoolean("ROOT", true) }
        fun Controller.setTag(tag: String) { args.putString("TAG", tag) }
        fun Controller.setRetain() { args.putBoolean("RETAIN", true)}
        fun Controller.setHome() { args.putBoolean("HOME", true) }

        when (it.id) {
            0 -> GalleryController().apply { setTag("1"); setRetain() }
            1 -> SignInController().apply { setTag("2"); setRetain() }
            else -> DebugController().apply { setTag("3") }
        }.apply {
            setRoot()
            if (it.id == settings.defaultId) {
                setHome()
                //home tab should be retain by default
                setRetain()
            }
        }
    }
}

