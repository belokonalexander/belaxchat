package com.belax.twos.di.retain

import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.navigation.Navigation
import com.belax.twos.base.navigation.NavigationImpl
import com.belax.twos.business.CameraInteractor
import com.belax.twos.business.CameraInteractorImpl
import com.belax.twos.business.PreviewInteractor
import com.belax.twos.business.PreviewInteractorImpl
import com.belax.twos.business.usecases.GetScrapbookUseCase
import com.belax.twos.data.CacheRepository
import com.belax.twos.data.GalleryRepository
import com.belax.twos.data.PermissionsRepository
import com.belax.twos.data.TimeRepository
import com.belax.twos.presentation.camera.CameraPresenter
import com.belax.twos.presentation.preview.PreviewPresenter

import com.bluelinelabs.conductor.Router
import dagger.Module
import dagger.Provides

@Module
class RetainModule(val router: Router) {

    @RetainScope
    @Provides
    fun provideRouter(): Navigation = NavigationImpl(router)

    @RetainScope
    @Provides
    fun provideCameraInteractor(
            permissionsRepository: PermissionsRepository,
            cacheRepository: CacheRepository,
            getScrapbookUseCase: GetScrapbookUseCase
    ): CameraInteractor = CameraInteractorImpl(permissionsRepository, getScrapbookUseCase, cacheRepository)

    @RetainScope
    @Provides
    fun provideCameraPresenter(
            cameraInteractor: CameraInteractor,
            rxSchedulers: RxSchedulers,
            navigation: Navigation
    ): CameraPresenter = CameraPresenter(cameraInteractor, rxSchedulers, navigation)

    @RetainScope
    @Provides
    fun providesPreviewPresenter(
            previewInteractor: PreviewInteractor,
            rxSchedulers: RxSchedulers,
            navigation: Navigation
    ): PreviewPresenter = PreviewPresenter(previewInteractor, rxSchedulers, navigation)

    @RetainScope
    @Provides
    fun providesPreviewInteractor(
            galleryRepository: GalleryRepository,
            timeRepository: TimeRepository,
            cacheRepository: CacheRepository,
            permissionsRepository: PermissionsRepository
    ): PreviewInteractor = PreviewInteractorImpl(galleryRepository, timeRepository, cacheRepository, permissionsRepository)

}