package com.belax.twos.di.home

import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.base.navigation.TabbedNavigation
import com.belax.twos.business.GalleryInteractor
import com.belax.twos.business.GalleryInteractorImpl
import com.belax.twos.data.GalleryRepository
import com.belax.twos.data.TimeRepository
import com.belax.twos.presentation.gallery.GalleryPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by Alexander on 18.03.2018.
 */
@Module
class GalleryModule {

    @HomeScope
    @Provides
    fun provideGalleryInteractor(
            repository: GalleryRepository,
            timeRepository: TimeRepository
    ): GalleryInteractor = GalleryInteractorImpl(repository)

    @HomeScope
    @Provides
    fun provideGalleryPresenter(
            interactor: GalleryInteractor,
            rxSchedulers: RxSchedulers,
            navigation: TabbedNavigation
    ): GalleryPresenter = GalleryPresenter(interactor, rxSchedulers, navigation)

}