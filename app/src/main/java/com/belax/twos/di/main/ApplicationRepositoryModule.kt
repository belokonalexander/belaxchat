package com.belax.twos.di.main

import android.content.Context
import com.belax.twos.AppDatabase
import com.belax.twos.business.usecases.GetScrapbookUseCase
import com.belax.twos.business.usecases.GetScrapbookUseCaseImpl
import com.belax.twos.data.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationRepositoryModule {

    @Singleton
    @Provides
    fun provideHardwareRepository(context: Context): HardwareRepository = HardwareRepositoryImpl(context)

    @Singleton
    @Provides
    fun provideCameraRepository(): CameraRepository = CameraRepositoryImpl()

    @Singleton
    @Provides
    fun provideGalleryRepository(
            context: Context,
            database: AppDatabase
    ): GalleryRepository = GalleryRepositoryImpl(context, database)

    @Singleton
    @Provides
    fun provideTimeRepository(): TimeRepository = TimeRepositoryImpl()

    @Singleton
    @Provides
    fun provideCacheRepository(context: Context): CacheRepository = CacheRepositoryImpl(context)

    @Singleton
    @Provides
    fun providePermissionRepository(
            context: Context,
            activityProvider: ActivityProvider
    ): PermissionsRepository = PermissionsRepositoryImpl(context, activityProvider)

    @Singleton
    @Provides
    fun provideGetScrapbookUseCase(
            galleryRepository: GalleryRepository
    ): GetScrapbookUseCase = GetScrapbookUseCaseImpl(galleryRepository)

}