package com.belax.twos.di.main

import android.arch.persistence.room.Room
import android.content.Context
import com.belax.twos.AppDatabase
import com.belax.twos.Migration2to3
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.arch.schedulers.RxSchedulersImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ApplicationModule(private val context: Context) {

    @Singleton
    @Provides
    fun provideContext(): Context = context

    @Singleton
    @Provides
    fun provideDatabase(context: Context): AppDatabase = Room.databaseBuilder(context,
            AppDatabase::class.java, "main").addMigrations(Migration2to3()).build()

    @Singleton
    @Provides
    fun provideRxSchedulers(): RxSchedulers = RxSchedulersImpl()

    @Singleton
    @Provides
    fun provideActivityProvider(): ActivityProvider = ActivityProvider(null)

}