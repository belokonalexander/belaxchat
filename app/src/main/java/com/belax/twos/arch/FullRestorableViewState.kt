package com.belax.twos.arch

import android.os.Bundle
import android.os.Parcelable
import com.belax.twos.base.BaseView
import com.belax.twos.logExeption
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState

class FullRestorableViewState<S, V : BaseView>(var state: S) : RestorableViewState<V> where S : Parcelable {

    override fun apply(view: V, retained: Boolean) {
        // it's presenter responsibility
    }

    companion object {
        val STATE_KEY = "state"
    }

    private fun Parcelable.printData() = this.javaClass.declaredFields
            .filterNot { it.isSynthetic || arrayListOf("CREATOR", "serialVersionUID").contains(it.name) }
            .joinToString { it.name.plus(" -> ${it.apply { isAccessible = true }.get(this)}") }

    /**
     * called if we want to restore state from bundle (process killed)
     */
    override fun restoreInstanceState(input: Bundle?): RestorableViewState<V>? = input?.let {
        state = input.getParcelable(STATE_KEY)
        logExeption("restoreInstanceState $state")
        this
    }

    /**
     * called after onViewDestroy for saving state in bundle
     */
    override fun saveInstanceState(out: Bundle) {
        logExeption("saveInstanceState $state")
        out.putParcelable(STATE_KEY, state)
    }

    override fun toString(): String = state.printData()

}