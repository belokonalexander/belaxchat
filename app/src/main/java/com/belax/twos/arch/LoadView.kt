package com.belax.twos.arch

import com.belax.twos.base.BaseView

interface MvpLoadView : BaseView {
    fun updateProgress(show: Boolean)
}