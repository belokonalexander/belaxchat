package com.belax.twos.arch

import android.os.Parcelable
import android.view.View
import com.belax.twos.base.*
import io.reactivex.disposables.CompositeDisposable

/**
 * VS is just retain object which stored in Controller and can be saved into Bundle for handling process killing
 * because is stored in controller we can keep state for avoid config changes (or view recreating)
 * VS inflates view in (onCreateView; onAttach) -> inflaterApply called
 * VS saves state to bundle in (onDetach; onDestroyView) -> saveInstanceState called
 */
abstract class BaseMvpViewStateController<V : BaseView, P : BasePresenter<V, S, I>, S, I : BaseInput> : BaseController<V, P, FullRestorableViewState<S, V>, S, I>()
        where S : Parcelable {

    protected val composite = CompositeDisposable()

    /**
     * called after first create of ViewState (controller starts and there's no any state)
     * @see com.hannesdorfmann.mosby3.conductor.viewstate.delegate.MvpViewStateConductorDelegateCallback
     */
    final override fun onNewViewStateInstance() {
        super.onNewViewStateInstance()
    }

    /**
     *  called after view state was successfully restored
     */
    final override fun onViewStateInstanceRestored(instanceStateRetained: Boolean) {
        super.onViewStateInstanceRestored(instanceStateRetained)
    }

    override fun onDetach(view: View) {
        super.onDetach(view)
        composite.clear()
    }

    final override fun createViewState(): FullRestorableViewState<S, V> = FullRestorableViewState(createState())
    abstract fun createState() : S
}