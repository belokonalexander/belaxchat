package com.belax.twos.arch.schedulers

import io.reactivex.CompletableTransformer
import io.reactivex.Scheduler
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import java.util.concurrent.TimeUnit


abstract class RxSchedulers {

    abstract fun getMainThreadScheduler(): Scheduler
    abstract fun getIOScheduler(): Scheduler
    abstract fun getComputationScheduler(): Scheduler

    companion object {
        const val defaultTimeout = 5000L
    }

    fun <T> getIOToMainTransformer() = ObservableTransformer<T, T> {
        it.subscribeOn(getIOScheduler()).observeOn(getMainThreadScheduler())
    }

    fun getIOToMainTransformerCompletable() = CompletableTransformer {
        it.subscribeOn(getIOScheduler()).observeOn(getMainThreadScheduler())
    }

    fun getIOToMainTransformerCompletableLimited() = CompletableTransformer {
        it.subscribeOn(getIOScheduler()).timeout(defaultTimeout, TimeUnit.MILLISECONDS).observeOn(getMainThreadScheduler())
    }

    fun <T> getIOToMainTransformerLimited() = ObservableTransformer<T, T> {
        it.subscribeOn(getIOScheduler()).timeout(defaultTimeout, TimeUnit.MILLISECONDS).observeOn(getMainThreadScheduler())
    }

    fun <T> getMainToIOTransformer() = ObservableTransformer<T, T> {
        it.subscribeOn(getMainThreadScheduler()).observeOn(getIOScheduler())
    }

    fun <T> getIOToMainTransformerSingle() = SingleTransformer<T, T> {
        it.subscribeOn(getIOScheduler()).observeOn(getMainThreadScheduler())
    }

    fun <T> getIOToMainTransformerSingleLimited() = SingleTransformer<T, T> {
        it.subscribeOn(getIOScheduler()).timeout(defaultTimeout, TimeUnit.MILLISECONDS).observeOn(getMainThreadScheduler())
    }

    fun <T> getMainToIOTransformerSingle() = SingleTransformer<T, T> {
        it.subscribeOn(getMainThreadScheduler()).observeOn(getIOScheduler())
    }


}