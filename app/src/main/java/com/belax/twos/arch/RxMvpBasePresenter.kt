package com.belax.twos.arch

import android.os.Parcelable
import com.belax.twos.arch.schedulers.RxSchedulers
import com.belax.twos.arch.schedulers.RxSchedulersImpl
import com.belax.twos.base.*
import com.belax.twos.base.navigation.Navigation
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class RxMvpBasePresenter<V : BaseView, S : Parcelable, I : BaseInput>(
        protected val schedulers: RxSchedulers = RxSchedulersImpl(),
        navigation: Navigation,
        protected val mapper: RxMapper = RxMapper()
) : BasePresenter<V, S, I>(navigation) {

    enum class TaskType { START_NEW, IGNORE }

    private val composite = CompositeDisposable()
    private val tasks = mutableMapOf<String, Disposable>()

    override fun destroy() {
        super.destroy()
        composite.clear()
        tasks.forEach({ (_, v) -> v.dispose() })
    }

    fun Disposable.watchPresenter(): Disposable {
        return composite.add(this).let { this }
    }

    @Synchronized
    private fun onSubscribeTag(tag: String, taskType: TaskType, subscribeAction: () -> Disposable) {
        val current = tasks[tag]
        if (current == null || current.isDisposed) {
            tasks[tag] = subscribeAction()
        } else {
            when (taskType) {
                TaskType.IGNORE -> { }
                TaskType.START_NEW -> {
                    current.dispose()
                    tasks[tag] = subscribeAction()
                }
            }
        }
    }

    protected fun tasksSnapshot() = when {
        tasks.isEmpty() -> "tasks store is empty"
        else -> tasks.toList()
                .map { (k, v) -> Pair(k, if (v.isDisposed) "completed" else "running") }
                .joinToString { (k, v) -> "$k -> [$v]" }
    }

    fun <T> Observable<T>.subscribeTag(
            tag: String,
            taskType: TaskType = TaskType.IGNORE,
            onNext: (T) -> Unit = { },
            onError: (Throwable) -> Unit = { },
            onComplete: () -> Unit = { }
    ) {
        onSubscribeTag(tag, taskType, { subscribe(onNext, onError, onComplete) })
    }

    fun <T> Single<T>.subscribeTag(
            tag: String,
            taskType: TaskType = TaskType.IGNORE,
            onSuccess: (T) -> Unit = { },
            onError: (Throwable) -> Unit = { }
    ) {
        onSubscribeTag(tag, taskType, { subscribe(onSuccess, onError) })
    }

    fun Completable.subscribeTag(
            tag: String,
            taskType: TaskType = TaskType.IGNORE,
            onComplete: () -> Unit = { },
            onError: (Throwable) -> Unit = { }
    ) {
        onSubscribeTag(tag, taskType, { subscribe(onComplete, onError) })
    }

    fun <T> Single<T>.progressTaskWithSubscribe(
            onSuccess: (T) -> Unit = {},
            onError: (Throwable) -> Unit = {}
    ): Disposable = doOnSubscribe {
        setViewInProgress()
    }.doOnError {
        setViewOutProgress()
        onError(it)
    }.subscribe { t ->
        setViewOutProgress()
        onSuccess(t)
    }

    private fun setViewInProgress() {
        (vs as? ProgressViewState)?.progress = true
        ifViewAttached { it.inProgress() }
    }

    private fun setViewOutProgress() {
        (vs as? ProgressViewState)?.progress = false
        ifViewAttached { it.outProgress() }
    }

    fun isTaskInWorkState(tag: String): Boolean = tasks[tag]?.let { !it.isDisposed } ?: false
}


