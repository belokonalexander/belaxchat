package com.belax.twos.arch.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RxSchedulersImpl : RxSchedulers() {
    override fun getMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()
    override fun getIOScheduler(): Scheduler = Schedulers.io()
    override fun getComputationScheduler() = Schedulers.computation()
}

class RxSchedulersTest : RxSchedulers() {
    override fun getMainThreadScheduler(): Scheduler = Schedulers.trampoline()
    override fun getIOScheduler(): Scheduler = Schedulers.trampoline()
    override fun getComputationScheduler() = Schedulers.trampoline()
}