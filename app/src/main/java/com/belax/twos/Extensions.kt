package com.belax.twos

import android.app.Activity
import android.view.View
import android.widget.TextView
import com.bluelinelabs.conductor.Router
import timber.log.Timber
import java.lang.Exception

fun <T> T.log(msg: String, prefix: String = "#1020"): T = this.apply { Timber.i("$prefix -> $msg") }
fun <T> T.logExeption(msg: String, prefix: String = "#1020"): T = this.apply { Timber.e("$prefix -> $msg") }
fun <T> T.logExeption(e: Throwable, prefix: String = "#1020"): T = this.apply { Timber.e(e, prefix) }

fun TextView.string() = this.text.toString()

fun catchIgnore(e: Exception, block: () -> Unit) {

}