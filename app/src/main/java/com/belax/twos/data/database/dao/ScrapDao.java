package com.belax.twos.data.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.util.Log;

import com.belax.twos.data.database.Scrap;
import com.belax.twos.data.database.Scrapbook;

import java.util.Date;

import io.reactivex.Single;

@Dao
public abstract class ScrapDao {
    @Insert
    public abstract long insert(Scrap scrap);

    @Query("UPDATE Scrapbook SET updated = :updated WHERE id = :id")
    public abstract void updateScrapbook(int id, Date updated);

    @Insert
    public abstract long insert(Scrapbook scrapbook);

    @Query("SELECT * FROM scrap WHERE id=:id")
    public abstract Single<Scrap> getScrap(int id);

    @Query("SELECT * FROM scrap WHERE scrapbookId=:scrapbookId ORDER BY created DESC LIMIT 1")
    public abstract Single<Scrap> getTopScrap(int scrapbookId);

    @Transaction
    public long insertAndUpdateScrapbook(Scrap scrap) {
        if (scrap.getScrapbookId() == Scrapbook.EMPTY_VAL) {
            long scrapbookId = insert(new Scrapbook());
            scrap.setScrapbookId((int) scrapbookId);
        } else {
            updateScrapbook(scrap.getScrapbookId(), new Date());
        }
        return insert(scrap);
    }

}
