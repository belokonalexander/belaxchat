package com.belax.twos.data

import android.content.Context
import com.belax.twos.logExeption
import io.reactivex.Completable
import io.reactivex.Single
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

interface CacheRepository {
    fun add(name: String, byteArray: ByteArray): Completable
    fun get(name: String): Single<ByteArray>
}

class CacheRepositoryImpl(private val context: Context) : CacheRepository {
    override fun add(name: String, byteArray: ByteArray): Completable {
        return Completable.fromAction {
            File( context.cacheDir, name).let {
                logExeption("write -> " + it.absolutePath)
                val os = FileOutputStream(it)
                os.write(byteArray)
                os.flush()
                os.close()
            }
        }
    }

    override fun get(name: String): Single<ByteArray> {
        return Single.fromCallable {
            val file = File(context.cacheDir, name)
            logExeption("read -> " + file.absolutePath)
            val inputStram = FileInputStream(file)
            val data = ByteArray(file.length().toInt())
            inputStram.read(data)
            inputStram.close()
            return@fromCallable data
        }
    }
}