package com.belax.twos.data

import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

interface AuthRepository {
    fun signIn(login: String, password: String): Single<FirebaseUser>
    fun requestPhoneCode(phoneNumber: String): Single<String>
    fun signUpWithCode(verificationId: String, code: String): Single<FirebaseUser>
}

class AuthRepositoryImpl(private val auth: FirebaseAuth, private val phoneAuth: PhoneAuthProvider) : AuthRepository{

    override fun signUpWithCode(verificationId: String, code: String): Single<FirebaseUser> = Single.create({ source ->
        auth.signInWithCredential(PhoneAuthProvider.getCredential(verificationId, code)).addOnCompleteListener {
            when (it.isSuccessful) {
                true -> source.onSuccess(auth.currentUser!! )
                else -> source.onError(it.exception ?: UnknownError())
            }
        }
    })

    override fun signIn(login: String, password: String): Single<FirebaseUser> = Single.create({ source ->
            Timber.i("Firebase auth current user ${ auth.currentUser?.email }")
            auth.signInWithEmailAndPassword(login, password)
                    .addOnCompleteListener {
                        when (it.isSuccessful) {
                            true -> source.onSuccess(auth.currentUser!!)
                            else -> source.onError(it.exception ?: UnknownError())
                        }
                    }
        })

    override fun requestPhoneCode(phoneNumber: String): Single<String> = Single.create({ source ->
        phoneAuth.verifyPhoneNumber(phoneNumber, 60, TimeUnit.SECONDS, Executor { it.run() }, object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential?) {
                Timber.i("onVerificationCompleted $p0")
            }

            override fun onVerificationFailed(p0: FirebaseException?) {
                Timber.i("onVerificationFailed $p0")
                source.onError(p0!!)
            }

            override fun onCodeSent(p0: String?, p1: PhoneAuthProvider.ForceResendingToken?) {
                super.onCodeSent(p0, p1)
                Timber.i("On code sent: $p0 ---> $p1")
                source.onSuccess(p0!!)
            }
        })
    })
}