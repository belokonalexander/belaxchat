package com.belax.twos.data.database

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

import java.util.Date

@Entity
data class Scrapbook (
    @PrimaryKey(autoGenerate = true)
    var id: Int = EMPTY_VAL,
    var title: String = "",
    var description: String = "",
    var created: Date = Date(),
    var updated: Date = Date()
) {
    companion object {
        const val EMPTY_VAL = 0
    }
}
