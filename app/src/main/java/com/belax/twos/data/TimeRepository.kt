package com.belax.twos.data

interface TimeRepository {
    fun currentTime(): Long
}

class TimeRepositoryImpl : TimeRepository {
    override fun currentTime(): Long {
        return System.currentTimeMillis()
    }
}