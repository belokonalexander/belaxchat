package com.belax.twos.data

import android.content.Context
import android.media.MediaScannerConnection
import android.os.Environment
import com.belax.twos.AppDatabase
import com.belax.twos.R
import com.belax.twos.data.database.Scrap
import com.belax.twos.data.database.ScrapbookAndScraps
import io.reactivex.Completable
import io.reactivex.Single
import java.io.File
import java.io.FileOutputStream

/**
 * Created by Alexander on 18.03.2018.
 */

interface GalleryRepository {
    fun savePicture(name: String, byteArray: ByteArray): Single<String>
    fun deletePicture(name: String): Completable
    fun existsPicture(name: String): Completable
    fun getScrapbooks(): Single<List<ScrapbookAndScraps>>
    fun getScrap(id: Int): Single<Scrap>
    fun getTopScrap(scrapbookId: Int): Single<Scrap>
    fun createScrap(scrapbookId: Int, picture: String): Single<Scrap>
    fun getScrapbook(scrapbookId: Int): Single<ScrapbookAndScraps>
}

class GalleryRepositoryImpl(
        private val context: Context,
        private val database: AppDatabase
) : GalleryRepository {

    override fun getScrapbook(scrapbookId: Int) = database.scrapbookDao().getScrapbook(scrapbookId)

    override fun getScrapbooks(): Single<List<ScrapbookAndScraps>> {
        return database.scrapbookDao().getScrapbooks()
    }

    override fun getTopScrap(scrapbookId: Int) = database.scrapDao().getTopScrap(scrapbookId)
    override fun getScrap(id: Int) = database.scrapDao().getScrap(id)
    override fun createScrap(scrapbookId: Int, picture: String) = Single.fromCallable {
        Scrap(scrapbookId = scrapbookId, picture = picture).apply {
            id = database.scrapDao().insertAndUpdateScrapbook(this).toInt()
        }
    }



    private fun getTargetFile(name: String): File {
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File(path, "${context.resources.getString(R.string.system_dir)}//$name.jpeg")
    }

    override fun savePicture(name: String, byteArray: ByteArray): Single<String> = Single.fromCallable {
        val file = getTargetFile(name)
        file.parentFile.mkdirs()
        val os = FileOutputStream(file)
        os.write(byteArray)
        os.flush()
        os.close()
        MediaScannerConnection.scanFile(context, arrayOf(file.toString()), null, null)
        return@fromCallable file.path
    }

    override fun deletePicture(name: String): Completable = Completable.fromAction {
        getTargetFile(name).delete()
    }

    override fun existsPicture(name: String) = Completable.fromAction {
        getTargetFile(name).exists()
    }

}