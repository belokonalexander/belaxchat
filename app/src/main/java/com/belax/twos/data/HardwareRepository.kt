package com.belax.twos.data

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat


/**
 * Created by Alexander on 09.02.2018.
 */

interface HardwareRepository {
    fun checkCameraHardware(): Boolean
}

class HardwareRepositoryImpl(private val context: Context) : HardwareRepository {
    override fun checkCameraHardware(): Boolean = context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
}