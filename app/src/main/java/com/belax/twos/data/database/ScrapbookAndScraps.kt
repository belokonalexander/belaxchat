package com.belax.twos.data.database

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

data class ScrapbookAndScraps(
        @Embedded var scrapbook: Scrapbook = Scrapbook(),
        @Relation(parentColumn = "id", entityColumn = "scrapbookId", entity = Scrap::class)
        var scraps: List<Scrap> = emptyList()
)