package com.belax.twos.data

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.belax.twos.di.main.ActivityProvider
import io.reactivex.Single


/**
 * Created by Alexander on 09.02.2018.
 */
object PermissionCode {
    val CAMERA_REQUEST_CODE = 1
    val WRITE_EXTERNAL_REQUEST_CODE = 2
}

enum class PermissionStatus {
    ALLOWED, DISABLED
}

interface PermissionsRepository {
    fun getCameraPermission(): PermissionStatus
    fun getStoragePermission(): PermissionStatus
    fun requestPermission(permissions: Array<String>, requestCode: Int): Single<PermissionStatus>
}

class PermissionsRepositoryImpl(
        private val context: Context,
        private val activityProvider: ActivityProvider
) : PermissionsRepository {

    override fun requestPermission(permissions: Array<String>, requestCode: Int): Single<PermissionStatus> {
        activityProvider.currentActivity?.let { activity ->
            ActivityCompat.requestPermissions(activity, permissions, requestCode)
            return activity.permissionsChanges
                    .filter({ it -> it.first == requestCode })
                    .firstElement()
                    .map { it.second }.toSingle()
        }

        //activity is not attached now
        return Single.just(PermissionStatus.DISABLED)
    }

    override fun getStoragePermission(): PermissionStatus = when (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
        PackageManager.PERMISSION_GRANTED -> PermissionStatus.ALLOWED
        else -> PermissionStatus.DISABLED
    }

    override fun getCameraPermission(): PermissionStatus = when (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)) {
            PackageManager.PERMISSION_GRANTED -> PermissionStatus.ALLOWED
            else -> PermissionStatus.DISABLED
    }
}