package com.belax.twos.data.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

import java.util.Date

@Entity(foreignKeys = [(ForeignKey(entity = Scrapbook::class, parentColumns = arrayOf("id"),
        childColumns = arrayOf("scrapbookId")))],
        indices = [(Index(value = ["scrapbookId"]))])
data class Scrap (
    @PrimaryKey(autoGenerate = true)
    var id: Int = EMPTY_VAL,
    var title: String = "",
    var description: String = "",
    var picture: String = "",
    var scrapbookId: Int = Scrapbook.EMPTY_VAL,
    var created: Date = Date()
) {
    companion object {
        const val EMPTY_VAL = 0
    }
}
