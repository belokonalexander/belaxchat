package com.belax.twos.data.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.belax.twos.data.database.Scrapbook;
import com.belax.twos.data.database.ScrapbookAndScraps;

import java.util.Date;
import java.util.List;

import io.reactivex.Single;

@Dao
public interface ScrapbookDao {
    @Transaction
    @Query("SELECT * FROM Scrapbook ORDER BY updated DESC")
    Single<List<ScrapbookAndScraps>> getScrapbooks();

    @Transaction
    @Query("SELECT * FROM Scrapbook WHERE id=:id")
    Single<ScrapbookAndScraps> getScrapbook(int id);
}
