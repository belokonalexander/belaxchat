package com.belax.twos

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by belokon on 07.12.17.
 */

fun <T> Observable<T>.log(prefix: String = "", formatter: (T) -> String? = { it.toString() }): Observable<T> = doOnNext({
    Timber.i("$prefix${formatter(it)}")
})

fun Disposable.toComposite(composite: CompositeDisposable) = composite.add(composite).let { this }

fun <T> Observable<T>.beforeSubscribe(action: () -> Unit): Observable<T> = action().let { this }

fun delayedAction(delay: Long, scheduler: Scheduler, action: () -> Unit) {
    Single.just(Unit).delay(delay, TimeUnit.MILLISECONDS).observeOn(scheduler).subscribe { _ -> action() }
}

fun <T> cachedSingle(
        item: T?,
        target: Single<T>,
        predicate: (T) -> Boolean = { true}
): Single<T> = if (item != null && predicate(item)) Single.just(item) else target

fun Disposable?.whenNotRunning(action: () -> Unit) {
    if (this == null || isDisposed) {
        action.invoke()
    }
}