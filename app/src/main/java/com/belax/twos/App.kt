package com.belax.twos

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import com.belax.twos.base.BaseActivity
import com.belax.twos.di.retain.RetainComponent
import com.belax.twos.di.retain.RetainModule
import com.belax.twos.di.home.HomeComponent
import com.belax.twos.di.home.HomeModule
import com.belax.twos.di.main.ApplicationComponent
import com.belax.twos.di.main.ApplicationModule
import com.belax.twos.di.main.DaggerApplicationComponent
import com.bluelinelabs.conductor.Router
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber


class App : Application() {
    private var componentHelper: ComponentHelper? = null

    companion object {
        fun get(context: Context) = context.applicationContext as App
    }

    fun component(): ComponentHelper {
        return componentHelper ?: ComponentHelper(DaggerApplicationComponent.builder().applicationModule(
                ApplicationModule(this)).build()).apply { componentHelper = this }
    }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        initTimber()
        setActivityListener()
        LeakCanary.install(this)
        Stetho.initializeWithDefaults(this)
    }

    private fun setActivityListener() {
        registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks{
            override fun onActivityStarted(activity: Activity?) {
                if (activity is BaseActivity) {
                    component().applicationComponent.getActivityProvider().update(activity)
                }
            }

            override fun onActivityStopped(activity: Activity?) {
                if (activity is BaseActivity) {
                    component().applicationComponent.getActivityProvider().update(null)
                }
            }

            //region Unused callbacks
            override fun onActivityPaused(activity: Activity?) {

            }

            override fun onActivityResumed(activity: Activity?) {

            }



            override fun onActivityDestroyed(activity: Activity?) {

            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {

            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {

            }
            //endregion
        })
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}

class ComponentHelper(val applicationComponent: ApplicationComponent) {
    private var retainComponent: RetainComponent? = null
    private var homeComponent: HomeComponent? = null

    fun getRetainComponent(router: Router): RetainComponent {
        return retainComponent ?: applicationComponent.plus(RetainModule(router)).apply {
            retainComponent = this
        }
    }

    fun getHomeComponent(router: Router, homeRouter: Router): HomeComponent {
        return homeComponent ?: getRetainComponent(router).plus(HomeModule(homeRouter)).apply {
            homeComponent = this
        }
    }

    fun destroyRetainComponent() {
        retainComponent = null
    }

    fun destroyHomeComponent() {
        homeComponent = null
    }
}