package com.belax.twos

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Parcel
import android.support.v4.view.AsyncLayoutInflater
import android.text.format.Formatter
import android.view.View
import android.view.ViewGroup
import com.belax.twos.base.BaseActivity
import com.belax.twos.presentation.home.HomeController
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    lateinit var router: Router
    val pool = HashMap<Int, View>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        log("MainActivity created, instance state: ${savedInstanceState}")
        router = Conductor.attachRouter(this, mainContainer, savedInstanceState)
        logExeption("MainActivity: ${this}, router: ${router}")
        when { !router.hasRootController() -> router.setRoot(RouterTransaction.with(HomeController())) }
    }

    override fun onStart() {
        super.onStart()
        initPool()
    }

    @SuppressLint("InflateParams")
    private fun initPool() {
        AsyncLayoutInflater(baseContext).inflate(R.layout.layout_camera, null, object : AsyncLayoutInflater.OnInflateFinishedListener {
            override fun onInflateFinished(view: View, resid: Int, parent: ViewGroup?) {
                log("onInflateFinished -> ${view}")
                pool[R.layout.layout_camera] = view
            }
        })
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        log("onRestoreInstanceState: ${getBundleSize(savedInstanceState)} - ${savedInstanceState}")
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        log("onSaveInstanceState: ${getBundleSize(outState)} - ${outState}")
    }

    private fun getBundleSize(bundle: Bundle?): String {
        val parcel = Parcel.obtain()
        parcel.writeValue(bundle)
        val bytes = parcel.marshall()
        parcel.recycle()
        return Formatter.formatFileSize(applicationContext, bytes.size.toLong())
    }

    override fun onBackPressed() {
        if (!router.handleBack()) {
            /* retain data will be destroyed after exit app by back pressed
              but application seems still be exists. So there can be bug with not actual component
              state and we should clean up this component in this way */
            App.get(this).component().destroyRetainComponent()
            super.onBackPressed()
        }
    }

    override fun onStop() {
        super.onStop()
        pool.clear()
    }
}

