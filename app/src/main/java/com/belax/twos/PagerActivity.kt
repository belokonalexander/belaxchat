package com.belax.twos

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.belax.twos.R
import com.belax.twos.log
import kotlinx.android.synthetic.main.activity_pager.*

class PagerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pager)
        val adapter = BasePagerAdapter()

        adapter.items = arrayListOf<TextView>().apply {
            repeat(12) {
                add(TextView(this@PagerActivity).apply { text = "SomeCategory$it" })
            }
        }
        //tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE)
        //tabLayout.set

        tabLayout.setupWithViewPager(pager)
        (pager as ViewPager).adapter = adapter

        //pager.postDelayed( { pager.setCurrentItem(10, false) }, 1400)
    }
}

class BaseViewPager : ViewPager {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

}

class BasePagerAdapter() : PagerAdapter() {

    var items = listOf<TextView>()

    override fun isViewFromObject(view: View, item: Any): Boolean {
        return item == view
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return items[position].text
    }
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        container.addView(items[position])
        logExeption("instantiateItem: $position, container: ${container}")
        return items[position]
    }

    override fun startUpdate(container: ViewGroup) {
        super.startUpdate(container)
        logExeption("start update")
    }

    override fun finishUpdate(container: ViewGroup) {
        super.finishUpdate(container)
        logExeption("finish update")
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(items[position])
        logExeption("destroyItem: $position")
    }

    override fun getCount(): Int {
        return items.count()
    }

}