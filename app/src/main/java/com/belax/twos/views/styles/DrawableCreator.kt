package com.belax.twos.views.styles

import android.graphics.drawable.Drawable

/**
 * Created by Alexander on 10.03.2018.
 */
interface DrawableCreator {
    fun getDrawable(): Drawable
}