package com.belax.twos.views.styles

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.StateListDrawable

/**
 * Created by Alexander on 10.03.2018.
 */
class PreLollipopRippleDrawable(private val normalColor: Int, private val  pressedColor: Int) : DrawableCreator {
    override fun getDrawable(): Drawable {
        return StateListDrawable().apply {
            addState( intArrayOf(android.R.attr.state_pressed),
                     ColorDrawable(pressedColor))
            addState( intArrayOf(android.R.attr.state_focused),
                     ColorDrawable(pressedColor))
            addState( intArrayOf(android.R.attr.state_activated),
                     ColorDrawable(pressedColor))
            addState( intArrayOf(), ColorDrawable(normalColor))
        }
    }
}