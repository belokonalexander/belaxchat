package com.belax.twos.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.belax.twos.R
import com.belax.twos.base.glide.GlideApp
import com.belax.twos.business.entities.ScrapbookUiModel
import kotlinx.android.synthetic.main.layout_gallery.view.*
import kotlinx.android.synthetic.main.view_latest.view.*

/**
 * Created by Alexander on 18.03.2018.
 */
class LatestScrapbookView : RelativeLayout {
    private var currentData: ScrapbookUiModel? = null
    private val view = LayoutInflater.from(context).inflate(R.layout.view_latest, rootView as ViewGroup)

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun bind(data: ScrapbookUiModel?, onButtonClick: (ScrapbookUiModel?) -> Unit) {
        currentData = data
        data?.let { model ->
            view.textTitle.text = model.title
            model.scraps.firstOrNull()?.let {
                GlideApp.with(context).load(it.picture).into(view.imagePreview)
            }
        }
        view.buttonUpdate.setOnClickListener { onButtonClick(currentData) }
    }
}