package com.belax.twos.views.styles

import android.annotation.TargetApi
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.ColorDrawable
import android.content.res.ColorStateList
import android.os.Build


/**
 * Created by Alexander on 10.03.2018.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class LollipopRippleDrawable(private val normalColor: Int, private val pressedColor: Int) : DrawableCreator{

    override fun getDrawable(): Drawable {
        return RippleDrawable(ColorStateList.valueOf(pressedColor), getColorDrawableFromColor(normalColor), null)
    }

    private fun getColorDrawableFromColor(color: Int): ColorDrawable {
        return ColorDrawable(color)
    }

}