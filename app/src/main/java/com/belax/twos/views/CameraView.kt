package com.belax.twos.views

import android.content.Context
import android.graphics.Color
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.util.AttributeSet
import android.view.TextureView
import android.widget.ImageButton
import android.widget.RelativeLayout
import com.belax.twos.*
import com.belax.twos.business.entities.CameraSettings
import com.belax.twos.business.entities.CameraType
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import com.belax.twos.whenNotRunning
import kotlin.math.abs

/**
 * Created by Alexander on 16.02.2018.
 */
class CameraView : RelativeLayout {

    private val preview = TextureView(context)
    private val buttonChangeCamera = createChangeCameraButton()
    private val prepareScheduler = Schedulers.io()
    private val mainScheduler = AndroidSchedulers.mainThread()
    private var camera: Camera? = null
    private var disposable: Disposable? = null
    private var currentSettings: CameraSettings? = null
    private var changeCameraListener: (CameraSettings) -> Unit = { }

    init {
        addView(preview)
        buttonChangeCamera.setVisible(false)
        addView(buttonChangeCamera, RelativeLayout.LayoutParams(resources.dpToPx(36), resources.dpToPx(36)).apply {
            addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
            addRule(RelativeLayout.ALIGN_PARENT_TOP)
            rightMargin = dpToPx(8)
            topMargin = dpToPx(8)
        })
        setOnClickListener {
            //https://stackoverflow.com/questions/17993751/whats-the-correct-way-to-implement-tap-to-focus-for-camera
            camera?.autoFocus { success, camera ->  }
        }
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private fun initSupportViews(camera: Camera, settings: CameraSettings) {
        buttonChangeCamera.setVisible(Camera.getNumberOfCameras() > 1)
    }

    private fun getStartListener(cameraSettings: CameraSettings): TextureView.SurfaceTextureListener? = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) { }
        override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) { }
        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
            return true
        }
        override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
            startPreview(cameraSettings)
        }
    }

    private fun startPreview(cameraSettings: CameraSettings) {
        disposable.whenNotRunning {
            when { currentSettings != cameraSettings -> stop() }
            currentSettings = cameraSettings
            disposable = Single.fromCallable {
                if (camera == null) {
                    camera = Camera.open(getTargetCameraInfo(cameraSettings.cameraType).second).apply {
                        initCameraParams(this, cameraSettings)
                        setPreviewTexture(preview.surfaceTexture)
                    }
                }
                return@fromCallable camera!!.apply { startPreview() }
            }.subscribeOn(prepareScheduler).observeOn(mainScheduler).subscribe({ it ->
                initSupportViews(it, cameraSettings)
            })
        }
    }

    private fun initCameraParams(camera: Camera, cameraSettings: CameraSettings) {
        val info = getTargetCameraInfo(cameraSettings.cameraType)
        val orientation = getTargetCameraOrientation(info.first, context.rotationAngle())
        camera.parameters = camera.parameters.apply {
            val previewSize = getTargetPreviewSizes(camera, orientation)
            setPreviewSize(previewSize.width, previewSize.height)
            val pictureSize = getTargetPictureSizes(camera, cameraSettings, previewSize)
            setPictureSize(pictureSize.width, pictureSize.height)
            log("picture size ${pictureSize.width} / ${pictureSize.height} ... preview size ${previewSize.width} / ${previewSize.height} orientation: ${orientation}")
            if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
                focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
        }
        camera.setDisplayOrientation(orientation)
    }

    /**
     * todo camera settings: need to prefer optimal resolution, not max
     */
    private fun getTargetPictureSizes(camera: Camera, settings: CameraSettings, previewSize: Camera.Size): Camera.Size {
        fun aspectRatio(size: Camera.Size) = size.width.toFloat() / size.height
        val previewAspectRatio = aspectRatio(previewSize)

        // we try to find picture size closable to preview size by aspect ratio
        val targetAspect = aspectRatio(camera.parameters.supportedPictureSizes
                .minBy { abs(previewAspectRatio - aspectRatio(it)) }!!)

        return camera.parameters.supportedPictureSizes
                .filter { aspectRatio(it) == targetAspect }
                .minBy { abs(it.width - previewSize.width * settings.resolutionFactor) }!!
    }

    /**
     * can be 0, 90, 180, 270
     */
    private fun getTargetCameraOrientation(info: Camera.CameraInfo, currentOrientation: Int): Int {
        return if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            (360 - (info.orientation + currentOrientation) % 360) % 360
        } else {
            (info.orientation - currentOrientation + 360) % 360
        }
    }

    private fun getTargetPreviewSizes(camera: Camera, orientation: Int): Camera.Size {
        val isReversed = orientation == 90 || orientation == 270
        val targetW = if (isReversed) resources.height() else resources.width()
        val targetH = if (isReversed) resources.width() else resources.height()
        return camera.parameters.supportedPreviewSizes.minBy {
            abs(it.width - targetW) + abs(it.height - targetH)
        } ?: camera.parameters.supportedPictureSizes[0]
    }

    /**
     * there is expected that at least one camera
     */
    private fun getTargetCameraInfo(cameraType: CameraType): Pair<Camera.CameraInfo, Int> {
        val defaultInfo = Camera.CameraInfo().apply { Camera.getCameraInfo(0, this) }
        for (i in 0..Camera.getNumberOfCameras()) {
            val cameraInfo = Camera.CameraInfo().apply { Camera.getCameraInfo(i, this) }
            if (cameraTypeMatching(cameraType, cameraInfo))
                return Pair(cameraInfo, i)
        }
        return Pair(defaultInfo, 0)
    }

    private fun cameraTypeMatching(cameraType: CameraType, info: Camera.CameraInfo): Boolean {
        if (cameraType == CameraType.MAIN && info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
            return true
        else if (cameraType == CameraType.ALTERNATIVE && info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
            return true

        return false
    }

    private fun createChangeCameraButton(): ImageButton = ImageButton(context).apply {
        setOnClickListener { currentSettings?.let { changeCameraListener(it) } }
        setImageDrawable(resources.getDrawable(R.mipmap.ic_launcher))
        background = getRippleDrawable(Color.WHITE, Color.RED)
    }

    /**
     * starts preview safely
     */
    fun start(cameraSettings: CameraSettings, changeCameraListener: (CameraSettings) -> Unit = { }) {
        this.changeCameraListener = changeCameraListener
        if (preview.isAvailable) {
            startPreview(cameraSettings)
            preview.surfaceTextureListener = null
        } else {
            preview.surfaceTextureListener = getStartListener(cameraSettings)
        }
    }

    /**
     * calls for take picture with success callback
     * todo is there need to add failureCallback? or some rx behaviour
     */
    fun takePicture(onSuccessTakePicture: (ByteArray, Int, CameraSettings) -> Unit) {
        try {
            camera?.takePicture(null, null, Camera.PictureCallback { bytes: ByteArray, camera: Camera ->
                currentSettings?.let {
                    onSuccessTakePicture(bytes, getTargetCameraOrientation(getTargetCameraInfo(it.cameraType).first, context.rotationAngle()), it)
                }
            })
        } catch (e: Exception) {
            logExeption("takePicture: ${e.message}")
        }
    }

    /**
     * stops preview and clear all data
     */
    fun stop() {
        buttonChangeCamera.setVisible(false)
        disposable?.dispose()
        camera?.stopPreview()
        camera?.release()
        camera = null
    }

}