package com.belax.twos.views.ui

import android.graphics.Rect
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.belax.twos.logExeption
import kotlin.math.max

/*
 * adds same offset to each element
 * the edge items simulate offsets by adding padding to recycler view for equal view sizes
 *
 * works with glm which has only one element in each span column
 */
class GridLMDecoration(totalOffset: Int, recyclerView: RecyclerView) : RecyclerView.ItemDecoration() {

    private val offset = totalOffset / 2

    init {
        recyclerView.setPadding(offset, offset, offset, offset)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        val spanCount = (parent.layoutManager as GridLayoutManager).spanCount
        val itemsCount = parent.adapter.itemCount
        val pos = parent.getChildAdapterPosition(view)

        val itemInLineNum = pos % spanCount
        val lineNum = pos / spanCount
        val lastLineNum = itemsCount / spanCount - if (itemsCount % spanCount == 0) 1 else 0
        val isLastInLine = pos % spanCount == spanCount - 1
        val isFirstInLine = pos % spanCount == 0

        outRect.bottom = offset
        outRect.top = offset
        outRect.right = offset
        outRect.left = offset

        logExeption("pos = $pos, linenum = $lineNum, itemInLineNum = $itemInLineNum, spanCount = $spanCount, lastLineNum = $lastLineNum, rect = $outRect", "offsets")
    }
}