package com.belax.twos.views

import android.content.Context
import android.graphics.Color
import android.support.graphics.drawable.VectorDrawableCompat
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.belax.twos.dpToPx
import com.belax.twos.getRippleDrawable

class BottomNavigationView : LinearLayout {
    private val items = mutableMapOf<Int, BottomItem>()
    private var selectedItem: BottomItem? = null
    private var defaultId: Int = -1

    var selectListener: (ItemMeta) -> Unit = { }
    var reselectListener: (ItemMeta) -> Unit = { }
    var deselectListener: (ItemMeta) -> Unit = { }

    init {
        orientation = HORIZONTAL
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    data class ItemMeta(val id: Int, val title: String, val image: Int)
    data class BottomNavSettings(val tabs: List<ItemMeta>, val defaultId: Int)

    fun setItems(settings: BottomNavSettings) {
        settings.tabs.forEach { it ->
            val itemView = createItemView(it)
            items[it.id] = itemView
            addItemToContainer(itemView)
        }
        defaultId = settings.defaultId
    }

    fun silenceSelect(id: Int?) {
        (if (id == null) items[defaultId] else items[id])?.onSilenceSelect()
    }

    private fun createItemView(item: ItemMeta): BottomItem = BottomItem(context).apply {
        setData(item)
    }

    private fun addItemToContainer(item: BottomItem) {
        addView(item, LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT).apply {
            weight = 1f
        })
    }

    private inner class BottomItem(context: Context?) : RelativeLayout(context) {
        lateinit var meta: ItemMeta
            private set
        private val imageView = ImageView(context).apply {
            id = View.generateViewId()
        }
        private val textView = TextView(context).apply {
            id = View.generateViewId()
            setSingleLine()
            ellipsize = TextUtils.TruncateAt.END
        }

        init {
            addView(imageView, RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
                addRule(RelativeLayout.CENTER_HORIZONTAL)
                topMargin = resources.dpToPx(8)
            })
            addView(textView, RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
                addRule(RelativeLayout.CENTER_HORIZONTAL)
                addRule(RelativeLayout.BELOW, imageView.id)
            })
        }

        fun setData(item: ItemMeta) {
            meta = item
            imageView.setImageDrawable(VectorDrawableCompat.create(context.resources, item.image, null))
            textView.text = item.title
            setOnClickListener { onSelectAction(this) }
            background = getRippleDrawable(Color.WHITE, Color.RED)
        }

        private fun onSelectAction(item: BottomItem) {
            if (item == selectedItem) {
                reselectListener(item.meta)
            } else {
                selectedItem?.let {
                    deselectListener(it.meta)
                    it.toNormalView()
                }
                selectListener(item.meta)
                selectedItem = this
                toSelectedView()
            }
        }

        fun onSilenceSelect() {
            selectedItem?.toNormalView()
            selectedItem = this
            toSelectedView()
        }

        private fun toSelectedView() {
            textView.setTextColor(Color.RED)
        }

        private fun toNormalView() {
            textView.setTextColor(Color.BLACK)
        }
    }
}